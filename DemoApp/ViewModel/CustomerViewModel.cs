﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using DemoApp.DataAccess;
using DemoApp.Model;
using DemoApp.Properties;

namespace DemoApp.ViewModel
{
    /// <summary>
    ///     A UI-friendly wrapper for a Customer object.
    /// </summary>
    public class CustomerViewModel : WorkspaceViewModel, IDataErrorInfo
    {
        public CustomerViewModel(Customer customer, CustomerRepository customerRepository)
        {
            if (customer == null) throw new ArgumentNullException("customer");
            if (customerRepository == null) throw new ArgumentNullException("customerRepository");

            this.customer = customer;
            this.customerRepository = customerRepository;
            customerType = Strings.CustomerViewModel_CustomerTypeOption_NotSpecified;
        }

        /// <summary>
        ///     Saves the customer to the repository.  This method is invoked by the SaveCommand.
        /// </summary>
        public void Save()
        {
            if (!customer.IsValid)
                throw new InvalidOperationException(Strings.CustomerViewModel_Exception_CannotSave);

            if (IsNewCustomer)
                customerRepository.AddCustomer(customer);

            OnPropertyChanged("DisplayName");
        }

        private readonly Customer customer;
        private readonly CustomerRepository customerRepository;
        private string customerType;
        private string[] customerTypeOptions;
        private bool isSelected;
        private RelayCommand saveCommand;

        public string Email
        {
            get { return customer.Email; }
            set
            {
                if (value == customer.Email)
                    return;

                customer.Email = value;

                OnPropertyChanged("Email");
            }
        }

        public string FirstName
        {
            get { return customer.FirstName; }
            set
            {
                if (value == customer.FirstName)
                    return;

                customer.FirstName = value;

                OnPropertyChanged("FirstName");
            }
        }

        public bool IsCompany
        {
            get { return customer.IsCompany; }
        }

        public string LastName
        {
            get { return customer.LastName; }
            set
            {
                if (value == customer.LastName)
                    return;

                customer.LastName = value;

                OnPropertyChanged("LastName");
            }
        }

        public double TotalSales
        {
            get { return customer.TotalSales; }
        }

        /// <summary>
        ///     Gets/sets a value that indicates what type of customer this is.
        ///     This property maps to the IsCompany property of the Customer class,
        ///     but also has support for an 'unselected' state.
        /// </summary>
        public string CustomerType
        {
            get { return customerType; }
            set
            {
                if (value == customerType || String.IsNullOrEmpty(value))
                    return;

                customerType = value;

                if (customerType == Strings.CustomerViewModel_CustomerTypeOption_Company)
                {
                    customer.IsCompany = true;
                }
                else if (customerType == Strings.CustomerViewModel_CustomerTypeOption_Person)
                {
                    customer.IsCompany = false;
                }

                OnPropertyChanged("CustomerType");

                // Since this ViewModel object has knowledge of how to translate
                // a customer type (i.e. text) to a Customer object's IsCompany property,
                // it also must raise a property change notification when it changes
                // the value of IsCompany.  The LastName property is validated 
                // differently based on whether the customer is a company or not,
                // so the validation for the LastName property must execute now.
                OnPropertyChanged("LastName");
            }
        }

        /// <summary>
        ///     Returns a list of strings used to populate the Customer Type selector.
        /// </summary>
        public string[] CustomerTypeOptions
        {
            get
            {
                if (customerTypeOptions == null)
                {
                    customerTypeOptions = new[]
                    {
                        Strings.CustomerViewModel_CustomerTypeOption_NotSpecified,
                        Strings.CustomerViewModel_CustomerTypeOption_Person,
                        Strings.CustomerViewModel_CustomerTypeOption_Company
                    };
                }
                return customerTypeOptions;
            }
        }

        public override string DisplayName
        {
            get
            {
                if (IsNewCustomer)
                {
                    return Strings.CustomerViewModel_DisplayName;
                }
                if (customer.IsCompany)
                {
                    return customer.FirstName;
                }
                return String.Format("{0}, {1}", customer.LastName, customer.FirstName);
            }
        }

        /// <summary>
        ///     Gets/sets whether this customer is selected in the UI.
        /// </summary>
        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (value == isSelected)
                    return;

                isSelected = value;

                OnPropertyChanged("IsSelected");
            }
        }

        /// <summary>
        ///     Returns a command that saves the customer.
        /// </summary>
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new RelayCommand(
                        param => Save(),
                        param => CanSave
                        );
                }
                return saveCommand;
            }
        }

        #region Private Helpers

        /// <summary>
        ///     Returns true if this customer was created by the user and it has not yet
        ///     been saved to the customer repository.
        /// </summary>
        private bool IsNewCustomer
        {
            get { return !customerRepository.ContainsCustomer(customer); }
        }

        /// <summary>
        ///     Returns true if the customer is valid and can be saved.
        /// </summary>
        private bool CanSave
        {
            get { return String.IsNullOrEmpty(ValidateCustomerType()) && customer.IsValid; }
        }

        #endregion // Private Helpers

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return (customer as IDataErrorInfo).Error; }
        }

        string IDataErrorInfo.this[string propertyName]
        {
            get
            {
                string error = null;

                if (propertyName == "CustomerType")
                {
                    // The IsCompany property of the Customer class 
                    // is Boolean, so it has no concept of being in
                    // an "unselected" state.  The CustomerViewModel
                    // class handles this mapping and validation.
                    error = ValidateCustomerType();
                }
                else
                {
                    error = (customer as IDataErrorInfo)[propertyName];
                }

                // Dirty the commands registered with CommandManager,
                // such as our Save command, so that they are queried
                // to see if they can execute now.
                CommandManager.InvalidateRequerySuggested();

                return error;
            }
        }

        private string ValidateCustomerType()
        {
            if (CustomerType == Strings.CustomerViewModel_CustomerTypeOption_Company ||
                CustomerType == Strings.CustomerViewModel_CustomerTypeOption_Person)
                return null;

            return Strings.CustomerViewModel_Error_MissingCustomerType;
        }

        #endregion // IDataErrorInfo Members
    }
}