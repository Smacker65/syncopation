﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Syncopation.Model.Eddb;
using Syncopation.Model.RedWizzard;
using Syncopation.Model.Starchart;

namespace Syncopation.Model
{
    public class StarChecker : SyncopatorCommand
    {
        public override string Name
        {
            get { return "StarChecker"; }
        }

        public override int Run(string compareToProvider, bool update)
        {
            Id = Name;
            CurrentProgress = 0;
            SendMessage("Start");

            var sourceSystems = new RedWizzardProvider(SharedData, Name);
            sourceSystems.Message += LocalMessage;
            sourceSystems.Progress += LocalProgressChanged;
            sourceSystems.ProgressIncrement = 25.0;

            ISystemProvider compareToSystems;

            if (compareToProvider=="Starchart")
            {
                compareToSystems = new StarChartProvider(SharedData, Name);
            }
            else
            {
                compareToSystems = new EddbProvider(SharedData, Name);
            }

            compareToSystems.Message += LocalMessage;
            compareToSystems.Progress += LocalProgressChanged;
            compareToSystems.ProgressIncrement = 25.0;

            Task.WaitAll(sourceSystems.Fetch(true), compareToSystems.Fetch(true));

            SendMessage("ScanSystems");
            SharedData.ScanSystems(compareToSystems);

            double increment = (100.0 - CurrentProgress) / sourceSystems.Systems.Count();
            var edits = update ? new EditRequest(StarChartProvider.BaseWebsite + "/api/edit") : null;

            SendMessage("Checking for differences");

            // Compare systems
            using (var writerMissing = new StreamWriter(Name + "_missing.txt", false))
            using (var writerCase = new StreamWriter(Name + "_case.txt", false))
            using (var writerPosition = new StreamWriter(Name + "_position.txt", false))
            using (var writerMismatch = new StreamWriter(Name + "_mismatch.txt", false))
            using (var writerRenames = new StreamWriter(Name + "_renames.txt", false))
            using (var writerUpper = new StreamWriter(Name + "_upper.txt", false))
            {
                writerMissing.WriteLine("Systems missing in {0}", compareToSystems.Source.Who);
                writerCase.WriteLine("System names differ by case {0} vs {1}", sourceSystems.Source.Who, compareToSystems.Source.Who);
                writerPosition.WriteLine("System positions differ {0} vs {1}", sourceSystems.Source.Who, compareToSystems.Source.Who);
                writerMismatch.WriteLine("System positions differ {0} vs {1}", sourceSystems.Source.Who, compareToSystems.Source.Who);
                foreach (var system in sourceSystems.Systems)
                {
                    try
                    {
                        if (SharedData.SystemExclusions.Contains(system.Name.ToUpperInvariant()))
                        {
                            writerMismatch.WriteLine("{0} {1} Skipping", system.Name, system.Position);
                            continue;
                        }

                        var currentSystem = compareToSystems.Systems.FirstOrDefault(s => String.Equals(s.Name, system.Name, StringComparison.InvariantCultureIgnoreCase));
                        if (currentSystem == null)
                        {
                            if (system.Position == null)
                            {
                                //writerMismatch.WriteLine("{0} {1}", system.Name, "[NULL]");
                                continue;
                            }
                            writerMissing.WriteLine("{0} {1}", system.Name, system.Position);

                            if (SharedData.SystemPositions.ContainsKey(system.Position))
                            {
                                writerMismatch.WriteLine("{0} {1} But clashes with {2} - \"{2}\", \"{0}\"", system.Name, system.Position, SharedData.SystemPositions[system.Position].Name);
                                writerRenames.WriteLine("{{\"{1}\", \"{0}\"}},", system.Name, SharedData.SystemPositions[system.Position].Name);
                                continue;
                            }

                            if (edits != null)
                            {
                                edits.CreateSystem(system, sourceSystems.Source);
                            }
                            continue;
                        }

                        if (!Equals(system.Name, currentSystem.Name))
                        {
                            writerCase.WriteLine("{0} vs {1}", system.Name, currentSystem.Name);
                            writerRenames.WriteLine("{{\"{1}\", \"{0}\"}},", system.Name, currentSystem.Name);
                        }

                        if (SystemIsUpper(currentSystem.Name, SharedData.SystemUpperExclusions))
                        {
                            writerUpper.WriteLine("{0}", currentSystem.Name);
                        }

                        if (system.Position == null)
                        {
                            writerMismatch.WriteLine("{0} {1} vs {2}", system.Name, "[NULL]", currentSystem.Position);
                        }
                        else if (!Equals(system.Position, currentSystem.Position))
                        {
                            if (SharedData.SystemPositions.ContainsKey(system.Position))
                            {
                                writerMismatch.WriteLine("{0} {1} vs {2} But clashes with {3}", system.Name, system.Position, currentSystem.Position, SharedData.SystemPositions[system.Position].Name);
                                continue;
                            }
                            writerPosition.WriteLine("{0} {1} vs {2}", system.Name, system.Position, currentSystem.Position);
                            if (edits != null)
                            {
                                edits.MoveSystem(currentSystem as Starchart.System, system.Position, sourceSystems.Source);
                            }
                        }
                    }
                    finally
                    {
                        IncrementProgress(increment);
                    }
                }
                var changes = 0;
                if (edits != null)
                {
                    edits.Execute();
                    changes = edits.TotalChanges;
                }
                SendMessage("Done");
                return changes;
            }
        }

        private bool SystemIsUpper(string systemName, IEnumerable<string> exclusions)
        {
            return Utils.IsAllUpper(systemName) && exclusions.All(exclusion => !systemName.Contains(exclusion));
        }



    }
}
