using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Syncopation.Model.Eddb;
using Syncopation.Model.PilotLog;
using Syncopation.Model.Starchart;
using Syncopation.Model.TradeDangerous;

namespace Syncopation.Model
{

    public sealed class Syncopator : SyncopatorCommand
    {
        private string baseProvider;

        public override string Name
        {
            get { return "Syncopator"; }
        }

        public override int Run(string provider, bool update)
        {
            baseProvider = provider;
            Id = Name;
            SendMessage("Start");

            SharedData.Clear();
            var providers = new List<IStationProvider>
            {
                new StarChartProvider(SharedData, Name),
                new EddbProvider(SharedData, Name),
                new PilotLogStationProvider(SharedData, Name),
                new TradeDangerousStationProvider(SharedData, Name, "TradeDangerous", string.Format(@"https://bitbucket.org/api/1.0/repositories/{0}/{1}/raw/{2}/{3}", "kfsone", "tradedangerous", "master", "data/Station.csv")),
                new TradeDangerousStationProvider(SharedData, Name, "Maddavo", @"http://www.davek.com.au/td/station.asp"),
            };

            int order = 1;
            foreach (var stationProvider in providers)
            {
                stationProvider.ProcessOrder = order++;
                stationProvider.ProgressIncrement = 40.0 / providers.Count;
                stationProvider.Message += LocalMessage;
                stationProvider.Progress += LocalProgressChanged;
            }

            SendMessage("Fetch All");
            FetchAllFiles(providers);

            SendMessage("Scan Systems");
            var baseStationProvider = providers.FirstOrDefault(p => String.Equals(p.Source.Who, baseProvider, StringComparison.InvariantCultureIgnoreCase));
            if (baseStationProvider == null)
            {
                SendMessage("Unknown Base Provider: " + baseProvider);
                return 0;
            }
            baseStationProvider.ProcessOrder = 0;
            SendMessage("Base Provider: " + baseStationProvider.Source.Who);


            SharedData.ScanSystems(baseStationProvider);

            SendMessage("Multi-Compare Systems");
            var systemMatches = MultiCompareSystems(providers, 5.0);
            SendMessage("Multi-Compare Stations");
            var stationMatches = MultiCompareStations(providers, 50.0);
            SendMessage("Apply Mismatches By Consensus");
            ApplyMismatchesByConsensus(stationMatches, 2.5);
            SendMessage("Create Update List");
            int changes = CreateUpdateList(update, systemMatches, stationMatches, 2.5);

            using (var w = new StreamWriter("station_corrections.txt", false))
            {
                foreach (var correction in SharedData.Corrections)
                {
                    w.WriteLine(correction);
                }
            }
            Utils.BackupFile("station_corrections.txt", Id);

            using (var w = new StreamWriter("station_case_corrections.txt", false))
            {
                foreach (var correction in SharedData.CaseCorrections)
                {
                    w.WriteLine(correction);
                }
            }
            Utils.BackupFile("station_case_corrections.txt", Id);

            var stationCountDiffs = new List<Tuple<int, Mismatch>>();
            using (var wc = new StreamWriter("mismatches.txt", false))
            using (var wn = new StreamWriter("mismatch_renames.txt", false))
            {
                wc.WriteLine(Mismatch.Header);
                foreach (var mismatch in SharedData.Mismatches.OrderBy(m => m.Key))
                {
                    wc.WriteLine(mismatch.Value.Line);
                    switch (mismatch.Value.ReasonCode)
                    {
                        case "NAME":
                            foreach (var line in mismatch.Value.RenameLines)
                            {
                                wn.WriteLine(line);
                            }
                            break;
                        case "#STATIONS":
                            int eddbCount;
                            int starchartCount;
                            if (int.TryParse(mismatch.Value.SourceValue("EDDB"), out eddbCount)
                             && int.TryParse(mismatch.Value.SourceValue("Starchart"), out starchartCount))
                            {
                                if (eddbCount > 0 && starchartCount > eddbCount)
                                {
                                    stationCountDiffs.Add(new Tuple<int, Mismatch>(starchartCount - eddbCount, mismatch.Value));
                                }
                            }
                            break;

                    }
                }
            }
            Utils.BackupFile("mismatches.txt", Id);

            using (var wf = new StreamWriter("FactionRenameBug.txt", false))
            {
                wf.WriteLine(Mismatch.Header);
                foreach (var stationCountDiff in stationCountDiffs.OrderByDescending(c => c.Item1).ThenBy(c => c.Item2.SystemName).Select(c => c.Item2))
                {
                    wf.WriteLine(stationCountDiff.Line);
                }

            }
            Utils.BackupFile("FactionRenameBug.txt", Id);


            SendMessage("Done");

            return changes;
        }


        private void FetchAllFiles(IEnumerable<IStationProvider> providers)
        {
            Task.WaitAll(providers.Select(provider => provider.Fetch()).ToArray());
        }


        private int CreateUpdateList(bool update, Dictionary<string, SystemMatch> systemMatches, Dictionary<string, StationMatch> stationMatches, double incrementAllocation)
        {
            var edits = update ? new EditRequest(StarChartProvider.BaseWebsite + "/api/edit") : null;

            CreateSystemUpdateList(systemMatches, edits, incrementAllocation / 3.0);
            CreateStationUpdateList(stationMatches, edits, incrementAllocation / 3.0);

            var changes = 0;
            if (edits != null)
            {
                edits.Execute();
                changes += edits.TotalChanges;
            }
            IncrementProgress(incrementAllocation / 3.0);
            return changes;
        }

        private void CreateSystemUpdateList(Dictionary<string, SystemMatch> systemMatches, EditRequest edits, double incrementAllocation)
        {
            using (var wu = new StreamWriter("system_updates.txt", false))
            {
                if (!systemMatches.Any())
                {
                    IncrementProgress(incrementAllocation);
                }
                var increment = incrementAllocation / systemMatches.Count;
                foreach (var systemMatch in systemMatches.Values.OrderBy(s => s.Name.ToUpperInvariant()))
                {
                    var systemKey = systemMatch.Name.ToUpperInvariant();
                    var system = SharedData.SystemNames[systemKey];
                    var changes = systemMatch.Updates(baseProvider);
                    foreach (var change in changes)
                    {
                        wu.WriteLine(change);
                    }
                    if (changes.Any())
                    {
                        var starchartSystem = system as Starchart.System;
                        SharedData.SystemNames[systemKey] = starchartSystem;
                        if (edits != null)
                        {
                            edits.UpdateSystem(starchartSystem, systemMatch);
                        }
                    }
                    IncrementProgress(increment);
                }
            }
            Utils.BackupFile("system_updates.txt", Id);
        }

        private void CreateStationUpdateList(Dictionary<string, StationMatch> stationMatches, EditRequest edits, double incrementAllocation)
        {
            using (var wm = new StreamWriter("station_matches.txt", false))
            using (var wu = new StreamWriter("station_updates.txt", false))
            {
                wm.WriteLine("System\tStation\tDist\tBlackMarket\tType\tSource");
                if (!stationMatches.Any())
                {
                    IncrementProgress(incrementAllocation);
                    return;
                }
                var increment = incrementAllocation / stationMatches.Count;
                foreach (var stationMatch in stationMatches.Values
                                                           .OrderBy(s => s.SystemName.ToUpperInvariant())
                                                           .ThenBy(s => s.Name.ToUpperInvariant()))
                {
                    var systemKey = stationMatch.SystemName.ToUpperInvariant();
                    var system = SharedData.SystemNames[systemKey];
                    wm.WriteLine(stationMatch.MatchLine);
                    var changes = stationMatch.Updates(baseProvider);
                    foreach (var change in changes)
                    {
                        wu.WriteLine(change);
                    }
                    if (changes.Any() && edits != null)
                    {
                        edits.UpdateStation(system as Starchart.System, stationMatch);
                    }
                    IncrementProgress(increment);
                }
            }
            Utils.BackupFile("station_matches.txt", Id);
            Utils.BackupFile("station_updates.txt", Id);
        }

        private void ApplyMismatchesByConsensus(IReadOnlyDictionary<string, StationMatch> matches, double incrementAllocation)
        {
            if (!SharedData.Mismatches.Any())
            {
                IncrementProgress(incrementAllocation);
                return;
            }
            var increment = incrementAllocation / SharedData.Mismatches.Count;
            foreach (var mismatch in SharedData.Mismatches.Values)
            {
                var consensus = mismatch.ConsensusValue;
                if (consensus.Types.Count() > 1)
                {
                    if (matches.ContainsKey(mismatch.Key))
                    {
                        var station = matches[mismatch.Key];
                        if (station == null) continue;
                        station.UpdateByConsensus(mismatch.ReasonCode, consensus);
                    }
                    else
                    {
                        // Add missing system?
                    }
                }
                IncrementProgress(increment);
            }
        }

        private Dictionary<string, SystemMatch> MultiCompareSystems(IEnumerable<IStationProvider> providers, double incrementAllocation)
        {
            var systemProviders = providers.OfType<ISystemProvider>().Where(p => p.Systems != null).ToList();

            var matches = new Dictionary<string, SystemMatch>();
            if (!systemProviders.Any())
            {
                IncrementProgress(incrementAllocation);
                return matches;
            }
            var providerIncrement = incrementAllocation/systemProviders.Count;
            foreach (var provider in systemProviders.OrderBy(p => p.ProcessOrder))
            {
                if (!provider.Systems.Any())
                {
                    IncrementProgress(providerIncrement);
                    continue;
                }
                var increment = providerIncrement/provider.Systems.Count();
                foreach (var s in provider.Systems)
                {
                    try
                    {
                        var system = s;
                        var key = system.Name.ToUpperInvariant();

                        if (SharedData.SystemExclusions.Contains(key))
                        {
                            SharedData.AddMismatch(system.Name, "SKIP", "Skipping System", null, null, system.Name, provider.Source);
                            continue;
                        }

                        if (!SharedData.SystemNames.ContainsKey(key))
                        {
                            SharedData.AddMismatch(system.Name, "NOSYS", "Missing System", null, null, system.Name, provider.Source);
                            continue;
                        }

                        if (matches.ContainsKey(key))
                        {
                            matches[key].Update(system, provider.Source, SharedData);
                            continue;
                        }
                        matches.Add(key, new SystemMatch(system, provider.Source));
                    }
                    finally
                    {
                        IncrementProgress(increment);
                    }
                }
            }
            return matches;
        }

        private Dictionary<string, StationMatch> MultiCompareStations(IEnumerable<IStationProvider> providers, double incrementAllocation)
        {
            var stationProviders = providers as IList<IStationProvider> ?? providers.ToList();
            var matches = new Dictionary<string, StationMatch>();

            if (!stationProviders.Any())
            {
                IncrementProgress(incrementAllocation);
                return matches;
            }

            var providerIncrement = incrementAllocation / stationProviders.Count;

            foreach (var provider in stationProviders.OrderBy(p => p.ProcessOrder))
            {
                if (!provider.Stations.Any())
                {
                    IncrementProgress(providerIncrement);
                    continue;
                }
                var increment = providerIncrement / provider.Stations.Count();
                foreach (var s in provider.Stations)
                {
                    try
                    {
                        var station = s;
                        var systemKey = station.SystemName.ToUpperInvariant();

                        if (!SharedData.SystemNames.ContainsKey(systemKey))
                        {
                            SharedData.AddMismatch(station.SystemName, station.Name, "NOSYS", "Missing System", null, null, station.Name, provider.Source);
                            continue;
                        }

                        var key = systemKey + @"/" + station.Name.ToUpperInvariant();

                        if (SharedData.SystemStationExclusions.Contains(key))
                        {
                            SharedData.AddMismatch(station.SystemName, station.Name, "SKIP", "Skippping Station", null, null, station.Name, provider.Source);
                            continue;
                        }

                        if (matches.ContainsKey(key))
                        {
                            matches[key].Update(station, provider.Source, SharedData);
                            continue;
                        }
                        if (DiscoverDuplicateStations(matches, systemKey, station, provider)) continue;
                        matches.Add(key, new StationMatch(station, provider.Source));
                    }
                    finally
                    {
                        IncrementProgress(increment);
                    }

                }
            }
            return matches;
        }

        private bool DiscoverDuplicateStations(Dictionary<string, StationMatch> matches, string systemKey, IStation station, IStationProvider provider)
        {
            foreach (var match in matches.Where(m => m.Value.SystemName.ToUpperInvariant() == systemKey))
            {
                if (String.Equals(station.Name, match.Value.Name, StringComparison.InvariantCultureIgnoreCase))
                {
                    SharedData.AddMismatch(station.SystemName, station.Name, "DUPN", "Possible duplicate stations detected", null,
                        null, station.RawName + "/" + match.Value.Name, provider.Source);
                    return true;
                }
                var compare = Utils.LevenshteinDistance(station.Name, match.Value.Name);
                if (compare > 0 && compare < 3)
                {
                    SharedData.AddMismatch(station.SystemName, station.Name, "NAME", "Close names detected", station.RawName,
                        provider.Source, match.Value.Name, match.Value.Sources[0]);
                    return true;
                }
            }
            return false;
        }

    }


}