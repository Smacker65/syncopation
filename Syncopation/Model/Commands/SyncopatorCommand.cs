﻿using System;

namespace Syncopation.Model
{
    public interface ISyncopatorCommand
    {
        int Run(string compareToProvider, bool update);
        event EventHandler<FineProgressChangedEventArgs> ProgressChanged;
        void IncrementProgress(double progress);
        event EventHandler<MessageEventArgs> Message;
        void SendMessage(string message);
        SharedData SharedData { set; }
        string Name { get; }
    }

    public abstract class SyncopatorCommand : ISyncopatorCommand
    {
        public SharedData SharedData { protected get; set; }
        public abstract string Name { get; }
        protected string Id;
        protected double CurrentProgress;

        public abstract int Run(string compareToProvider, bool update);

        protected void LocalProgressChanged(object sender, FineProgressChangedEventArgs args)
        {
            IncrementProgress(args.ProgressPercentage);
        }

        public event EventHandler<FineProgressChangedEventArgs> ProgressChanged = delegate { };

        public void IncrementProgress(double progress)
        {
            CurrentProgress += progress;
            ProgressChanged(this, new FineProgressChangedEventArgs(progress, Id));
        }

        public event EventHandler<MessageEventArgs> Message = delegate { };

        protected void LocalMessage(object sender, MessageEventArgs args)
        {
            SendMessage(args.Message);
        }

        public void SendMessage(string message)
        {
            Message(this, new MessageEventArgs(message, Id));
        }
    }
}