﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Syncopation.Model.Starchart;

namespace Syncopation.Model
{

    [DataContract]
    [DebuggerDisplay("Edit {Op,nq} {Position,nq} {Data,nq}")]
    public class Edit
    {

        public Edit()
        {
        }

        public Edit(string op, Starchart.System system)
            : this(op, system.Position, system)
        {
        }

        public Edit(string op, Position position, Starchart.System system)
            : this()
        {
            Op = op;
            Position = position;
            Key = new[] { position.x, position.y, position.z };
            Data = system;
        }

        [DataMember(Name = "op", Order = 1)]
        public string Op { get; set; }

        private Position Position { get; set; }

        [DataMember(Name = "key", Order = 2)]
        public double[] Key { get; set; }
        [DataMember(Name = "data", Order = 3, EmitDefaultValue = false)]
        public Starchart.System Data { get; set; }
    }

    [DataContract]
    [DebuggerDisplay("Edits {edits.Count,nq}")]
    public class Edits
    {
        public Edits(string source)
        {
            Source = source;
            edits = new List<Edit>();
        }

        public void AddEdit(string op, Starchart.System data)
        {
            edits.Add(new Edit(op, data));
        }

        public void AddEdit(string op, Position position, Starchart.System data = null)
        {
            edits.Add(new Edit(op, position, data));
        }

        public void Clear()
        {
            edits.Clear();
        }

        private readonly List<Edit> edits;

        [DataMember(Name = "source", Order = 1)]
        public string Source { get; set; }

        [DataMember(Name = "data", Order = 2)]
        public Edit[] Data
        {
            get { return edits.ToArray(); }
        }

        internal int Count
        {
            get { return edits.Count(); }
        }
    }

    public class EditRequest
    {
        public EditRequest(string editUri)
        {
            EditUri = editUri;
            edits = new Edits("Syncopation");
            TotalChanges = 0;
        }

        private void PostJson(string json)
        {

            //var json = "{\"J\":\"jj\", \"k\":\"kk\"}";
            //json = "{some:\"json data\"}";
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json; charset=utf-8";
                client.Encoding = Encoding.UTF8;
                try
                {
                    var response = client.UploadString(new Uri(EditUri), json);
                    var match = Regex.Match(response, @"""success"":(\d+)");
                    if (match.Success)
                    {
                        TotalChanges += int.Parse(match.Groups[1].Value);
                    }
                    Console.WriteLine("Response={0}", response);
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Error={0}", ex);           
                }
            }
           
        }

        private static void InsertCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Console.WriteLine("Error={0}", e.Error);           
            }
            else
            {
                Console.WriteLine("Response={0}", e.Result);
            }
        }

        private string ConvertToJson(Edits data)
        {
            using (var jsonStream = new MemoryStream())
            {
                var jsonSerializer = new DataContractJsonSerializer(typeof(Edits));
                jsonSerializer.WriteObject(jsonStream, data);

                jsonStream.Position = 0;
                using (var sr = new StreamReader(jsonStream))
                {
                    return sr.ReadToEnd();
                }
            }
        }

        public void DeleteSystem(Position pos)
        {
            edits.AddEdit("delete", pos);
        }

        public void UpdateStation(Starchart.System system, StationMatch station)
        {
            if (system == null) return;
            system.UpdateStation(station);
            UpdateSystem(system, station.Sources);
        }

        public void UpdateSystem(Starchart.System system, SystemMatch systemMatch)
        {
            system.UpdateSystem(systemMatch);
            system.Cmdr = ConstructCommander(systemMatch.Sources);
            edits.AddEdit("update", system);
            if (edits.Count >= MAX_EDITS) Execute();
        }

        public void UpdateSystem(Starchart.System system, IEnumerable<Source> sources)
        {
            system.Cmdr = ConstructCommander(sources);
            edits.AddEdit("update", system);
            if (edits.Count >= MAX_EDITS) Execute();
        }

        private string ConstructCommander(IEnumerable<Source> sources)
        {
            return "Syncopation [" + string.Join("+", sources.Where(s => s.Who != "Starchart").Select(s => s.Who).OrderBy(w => w)) + "]";
        }


        public void CreateSystem(ISystem system, Source source)
        {
            var newSystem = new Starchart.System(system, ConstructCommander(new[] { source }));
            AddSystem(newSystem);
        }

        public void AddSystem(Starchart.System system)
        {
            edits.AddEdit("add", system);
            if (edits.Count >= MAX_EDITS) Execute();
        }

        public void RenameSystem(Starchart.System system)
        {
            edits.AddEdit("rename", system);
            if (edits.Count >= MAX_EDITS) Execute();
        }


        public void MoveSystem(Starchart.System system, Position position, Source source)
        {
            var currentPosition = system.Position;
            system.Position = position;
            system.Cmdr = ConstructCommander(new[] {source});
            system.Modified = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ");

            edits.AddEdit("move", currentPosition, system);
            if (edits.Count >= MAX_EDITS) Execute();
        }


        public string EditUri { get; set; }
        public int TotalChanges { get; private set; }

        private readonly Edits edits;
        private const int MAX_EDITS = 1000;

        internal void Execute()
        {
            PostJson(ConvertToJson(edits));
            edits.Clear();
        }

        [DataContract]
        public class EditsFromLog
        {
            [DataMember(Name = "edits", Order = 1)]
            public EditFromLog[] Edits { get; set; }
        }

        [DataContract]
        public class EditFromLog
        {
            [DataMember(Name = "remoteAddr", Order = 1)]
            public string RemoteAddr { get; set; }
            [DataMember(Name = "time", Order = 2)]
            public string Time { get; set; }
            [DataMember(Name = "source", Order = 3)]
            public string Source { get; set; }
            [DataMember(Name = "data", Order = 4)]
            public Edit[] Edits { get; set; }
        }

        public void RerunFromArchive(StarChartProvider currentStations, string dataMasterLog)
        {
            string json;
            using (var reader = new StreamReader(dataMasterLog))
            {
                json = reader.ReadToEnd();
            }

            var editsLog = JsonConvert.DeserializeObject<EditsFromLog>(json);

            var systemEdits = new EditRequest(StarChartProvider.BaseWebsite + "/api/edit");

           // var liveStations = currentStations.AllStations;

            var logEdits = editsLog.Edits;
            foreach (var logEdit in logEdits)
            {
                foreach (var edit in logEdit.Edits)
                {
                    var system = edit.Data;
                    if (system == null) continue;
                    var liveSystem = currentStations.Systems.FirstOrDefault(s => s.Name == system.Name) as Starchart.System;
                    if (liveSystem == null)
                    {
                        Logger.Message("Skipping " + system.Name);
                        continue;
                    }
                    // Merge systems
                    var modified = false;
                    if (liveSystem.Allegiance == null && system.Allegiance != null)
                    {
                        liveSystem.Allegiance = system.Allegiance;
                        modified = true;
                    }
                    if (liveSystem.PrimaryEconomy == null && system.PrimaryEconomy != null)
                    {
                        liveSystem.PrimaryEconomy = system.PrimaryEconomy;
                        modified = true;
                    }
                    if (liveSystem.Government == null && system.Government != null)
                    {
                        liveSystem.Government = system.Government;
                        modified = true;
                    }
                    if (liveSystem.Population == 0 && system.Population > 0)
                    {
                        liveSystem.Population = system.Population;
                        modified = true;
                    }
                    if (liveSystem.RequiresPermit == null && system.RequiresPermit != null)
                    {
                        liveSystem.RequiresPermit = system.RequiresPermit;
                        modified = true;
                    }
                    if (system.Stations != null)
                    {
                        foreach (var station in system.Stations.Where(s => s.Inferred != true))
                        {
                            var liveStation = liveSystem.Stations.FirstOrDefault(s => s.Name == station.Name);
                            if (liveStation == null)
                            {
                                foreach (var s in liveSystem.Stations)
                                {
                                    var dist = Utils.LevenshteinDistance(s.Name, station.Name);

                                    if (dist < 2)
                                    {
                                        liveStation = s;
                                        break;
                                    }
                                    if (dist < 3)
                                    {

                                    }

                                }
                                if (liveStation == null) continue;
                            }

                            if (liveStation.Faction == null && station.Faction != null)
                            {
                                liveStation.Faction = station.Faction;
                                modified = true;
                            }
                            if (liveStation.Government == null && station.Government != null)
                            {
                                liveStation.Government = station.Government;
                                modified = true;
                            }
                            if (liveStation.Allegiance == null && station.Allegiance != null)
                            {
                                liveStation.Allegiance = station.Allegiance;
                                modified = true;
                            }
                            if (liveStation.Economies == null && station.Economies != null)
                            {
                                liveStation.Economies = station.Economies;
                                modified = true;
                            }
                            if (liveStation.Shipyard == null && station.Shipyard != null)
                            {
                                liveStation.Shipyard = station.Shipyard;
                                modified = true;
                            }
                            if (liveStation.Outfitting == null && station.Outfitting != null)
                            {
                                liveStation.Outfitting = station.Outfitting;
                                modified = true;
                            }
                            if (liveStation.Commodities == null && station.Commodities != null)
                            {
                                liveStation.Commodities = station.Commodities;
                                modified = true;
                            }
                        }
                    }

                    if (modified)
                    {
                        //systemEdits.UpdateSystem(liveSystem, null);
                        //edits.Execute();
                    }

                }
            }
            systemEdits.Execute();

        }

    }
}
