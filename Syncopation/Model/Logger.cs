﻿using System;
using System.Diagnostics;


namespace Syncopation.Model
{

    public static class Logger
    {

        public static void Message(string message)
        {
            Debug.WriteLine(message);
        }
    }

    public class FineProgressChangedEventArgs : EventArgs
    {
        public FineProgressChangedEventArgs(double progressPercentage, object userState)
        {
            ProgressPercentage = progressPercentage;
            UserState = userState;
        }

        public double ProgressPercentage { get; private set; }
        public object UserState { get; private set; }
    }


    public class MessageEventArgs : EventArgs
    {
        public MessageEventArgs(string message, object userState)
        {
            Message = message;
            UserState = userState;
        }
        public string Message { get; private set; }
        public object UserState { get; private set; }
    }

}
