using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace Syncopation.Model
{

    public interface IMatchValue<in TB> where TB : IEntity
    {
        string Name { get; }
        List<Source> Sources { get; }
        string Code { get; }
        string Description { get; }
        string ValueAsString { get; }
        void Update(TB entity, Source source, SharedData sharedData);

        void Set(string newValue, List<Source> sources);

        string SourcesForDisplay { get; }
    }

    [DebuggerDisplay("MatchValue {Code,nq} {Name,nq}={Value} [{Sources.Count,nq}")]
    public class MatchValue<T, TB> : IMatchValue<TB> where TB : IEntity
    {
        private readonly Func<TB, T> compareTo;
        private readonly Func<T, bool> isUnset;
        private readonly Func<T, T, bool> isDifferent;
        private readonly bool canUpdate;

        public MatchValue(string name, string code, string description, TB entity, Source source,
            Func<TB, T> compareTo,
            Func<T, bool> isUnset = null,
            Func<T, T, bool> isDifferent = null,
            bool canUpdate = true)
        {
            Name = name;
            Sources = new List<Source> { source };
            Code = code;
            Description = description;
            Value = compareTo(entity);
            this.canUpdate = canUpdate;
            this.compareTo = compareTo;
            this.isUnset = isUnset ?? (v => v == null);
            this.isDifferent = isDifferent ?? ((v1, v2) => !v1.Equals(v2));
        }

        public string Name { get; private set; }
        public T Value { get; private set; }
        public List<Source> Sources { get; private set; }
        public string Code { get; private set; }
        public string Description { get; private set; }

        public string ValueAsString { get { return Value==null ? "" : Value.ToString(); } }

        private bool IsDifferent(TB entity)
        {
            var v2 = compareTo(entity);
            return !isUnset(v2) && isDifferent(Value, v2);
        }

        public void Update(TB entity, Source source, SharedData sharedData)
        {
            if (canUpdate && isUnset(Value))
            {
                if (isUnset(compareTo(entity))) return;
                Value = compareTo(entity);
                Sources = new List<Source> { source };
            }
            else if (IsDifferent(entity))
            {
                var station = entity as IStation;
                if (station != null)
                {
                    sharedData.AddMismatch(station.SystemName, station.Name, Code, Description, ForDisplay(Value),
                        Sources[0], ForDisplay(compareTo(entity)), source);
                    return;
                }
                sharedData.AddMismatch(entity.Name, Code, Description, ForDisplay(Value),
                                       Sources[0], ForDisplay(compareTo(entity)), source);
            }
        }

        private string ForDisplay(T v)
        {
            return isUnset(v) ? "" : v.ToString();
        }

        private TypeConverter typeConverter;
        private TypeConverter TypeConverter
        {
            get
            {
                return typeConverter ?? (typeConverter = TypeDescriptor.GetConverter(typeof(T)));
            }
        }

        public void Set(string newValue, List<Source> sources)
        {

            // Value = ChangeType<T>(newValue);
            Value = (T)TypeConverter.ConvertFrom(newValue);
            Sources = sources;
        }

        public string SourcesForDisplay
        {
            get
            {
                if (Sources == null || Sources.Count == 0) return "Unknown";
                if (Sources.Count == 1) return Sources[0].Who;
                return string.Join("+", Sources.Select(s => s.Who));
            }    
        }
    }
}