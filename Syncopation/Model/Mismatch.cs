using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Syncopation.Model
{
    [DebuggerDisplay("Mismatch {Line}")]
    public class Mismatch
    {
        private static readonly List<Source> Sources;

        public Mismatch(string systemName, string stationName, string reasonCode,  string reason, string currentValue, Source currentSource)
        {
            Reason = reason;
            ReasonCode = reasonCode;
            SystemName = systemName;
            StationName = stationName;
            Trail = new Dictionary<Source, string>();
            if (!String.IsNullOrEmpty(currentValue) && currentSource!=null) AddDataPoint(currentValue, currentSource);
        }

        static Mismatch()
        {
            Sources = new List<Source>();
        }

        public void AddDataPoint(string currentValue, Source currentSource)
        {
            if (!Sources.Contains(currentSource)) Sources.Add(currentSource);
            if (Trail.ContainsKey(currentSource)) return;
            Trail.Add(currentSource, currentValue);
        }

        public ValueWithSources ConsensusValue
        {
            get
            {
                var uniqueValues = new Dictionary<string, List<Source>>();
                foreach (var point in Trail)
                {
                    if (!uniqueValues.ContainsKey(point.Value))
                    {
                        uniqueValues.Add(point.Value, new List<Source> { point.Key });
                        continue;
                    }
                    uniqueValues[point.Value].Add(point.Key);
                }
                if (!uniqueValues.Any()) return null;
                var highest = uniqueValues.OrderByDescending(v => MaxTypes(v.Value)).FirstOrDefault();
                return new ValueWithSources(highest.Key, highest.Value);
            }
        }

        private int MaxTypes(IEnumerable<Source> sources)
        {
            return sources.GroupBy(s => s.Type).OrderByDescending(t => t.Count()).Select(t => t.Count()).FirstOrDefault();
        }

        public string SystemName { get; set; }
        public string StationName { get; set; }
        public string Reason { get; set; }
        private Dictionary<Source, string> Trail { get; set; }
        public string ReasonCode { get; private set; }

        public string Line
        {
            get
            {
                var line = string.Join("\t", SystemName, StationName ?? "", ReasonCode);
                return Sources.OrderBy(s => s.Who).Aggregate(line, (current, source) => current + ("\t" + (Trail.ContainsKey(source) ? Trail[source] : "")));
            }
        }

        public static string Header
        {
            get
            {
                var line = string.Join("\t", "System", "Station", "Diff");
                return Sources.OrderBy(s => s.Who).Aggregate(line, (current, source) => current + ("\t" + source.Who));
            }
        }

        public string Key
        {
            get
            {
                if (StationName == null)
                {
                    return SystemName.ToUpperInvariant();
                }
                return SystemName.ToUpperInvariant() + @"/" + StationName.ToUpperInvariant();
            }
        }

        public IEnumerable<string> RenameLines
        {
            get
            {
                if (StationName == null)
                {
                    return (Trail.Values.Where(val => val != SystemName).Select(val => "\t" + SystemName + "," + val)).ToList();
                }
                return (Trail.Values.Where(val => val != StationName).Select(val => string.Join("\t", SystemName, StationName + "," + val))).ToList();
            }
        }

        public string SourceValue(string who)
        {
            return Trail.Where(t => t.Key.Who == who).Select(t => t.Value).FirstOrDefault();
        }
    }
}