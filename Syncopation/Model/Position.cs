using System;
using System.Diagnostics;

namespace Syncopation.Model
{
    [DebuggerDisplay("{ToString(),nq}")]
    public class Position : IComparable<Position>, IComparable, IEquatable<Position>
    {

        public Position(double? x, double? y, double? z)
        {
            if (!x.HasValue) throw new ArgumentNullException("x");
            if (!y.HasValue) throw new ArgumentNullException("y");
            if (!z.HasValue) throw new ArgumentNullException("z");

            this.x = x.Value;
            this.y = y.Value;
            this.z = z.Value;
        }

        public double x { get; private set; }
        public double y { get; private set; }
        public double z { get; private set; }

        private const double TOLERANCE = 1e-6;

        public bool Equals(Position other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return Math.Abs(other.x - x) < TOLERANCE && Math.Abs(other.y - y) < TOLERANCE && Math.Abs(other.z - z) < TOLERANCE;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Position)obj);
        }

        public override int GetHashCode()
        {
            var hash = x.GetHashCode();
            hash = ((hash << 5) + hash) ^ y.GetHashCode();
            hash = ((hash << 5) + hash) ^ z.GetHashCode();
            return hash;
        }

        public int CompareTo(Position other) 
        {
            var compare = x.CompareTo(other.x);
            if (compare != 0) return compare;
            compare = y.CompareTo(other.y);
            if (compare != 0) return compare;
            return z.CompareTo(other.z);
        }

        public int CompareTo(object obj)
        {
            if (obj != null && !(obj is Position)) throw new ArgumentException("Object must be of type Position.");
            return CompareTo(obj as Position);
        }

        public override string ToString()
        {
            return string.Format("[{0},{1},{2}]", x, y, z);
        }
    }
}