﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Syncopation.Model.Eddb
{
    [DebuggerDisplay("EddbSystem {Name}")]
    [DataContract]
    public class System : ISystem
    {
        public System()
        {
            Stations = new List<IStation>();
        }

        [DataMember(Name = "id", Order = 1)]
        public int Id { get; set; }
        [DataMember(Name = "name", Order = 2)]
        public string Name { get; set; }

        public string RawName { get; set; }

        [DataMember(Name = "x", Order = 3)]
        public double x { get; set; }
        [DataMember(Name = "y", Order = 4)]
        public double y { get; set; }
        [DataMember(Name = "z", Order = 5)]
        public double z { get; set; }

        public Position Position
        {
            get
            {
                return new Position(x, y, z);
            }
        }

        [DataMember(Name = "population", Order = 6)]
        public double? Population { get; set; }
        [DataMember(Name = "government", Order = 7)]
        public string Government { get; set; }
        [DataMember(Name = "allegiance", Order = 8)]
        public string Allegiance { get; set; }
        [DataMember(Name = "faction", Order = 9)]
        public string Faction { get; set; }
        [DataMember(Name = "state", Order = 10)]
        public string State { get; set; }
        [DataMember(Name = "security", Order = 11)]
        public string Security { get; set; }
        [DataMember(Name = "primary_economy", Order = 12)]
        public string PrimaryEconomy { get; set; }
        [DataMember(Name = "needs_permit", Order = 13)]
        public bool? NeedsPermit { get; set; }

        public bool? RequiresPermit
        {
            get { return NeedsPermit == true; }
            set { NeedsPermit = value ?? false; }
        }

        public bool DoneWithStations
        {
            get { return false; }
            set {  }
        }

        public List<IStation> Stations { get; private set; } 

        public int? StationCount
        {
            get
            {
                if (Stations == null) return null;
                return Stations.Count;
            }
        }
    }

    [DebuggerDisplay("EddbStation {Name}")]
    [DataContract]
    public class Station : IStation
    {
        [DataMember(Name = "id", Order = 1)]
        public int Id { get; set; }
        [DataMember(Name = "name", Order = 2)]
        public string Name { get; set; }
        [DataMember(Name = "system_id", Order = 3)]
        public int SystemId { get; set; }

        [DataMember(Name = "max_landing_pad_size", Order = 3)]
        public string MaxLandingPadSize { get; set; }
        [DataMember(Name = "distance_to_star", Order = 3)]
        public int? Distance { get; set; }

        [DataMember(Name = "faction", Order = 3)]
        public string Faction { get; set; }
        [DataMember(Name = "government", Order = 3)]
        public string Government { get; set; }
        [DataMember(Name = "allegiance", Order = 3)]
        public string Allegiance { get; set; }
        [DataMember(Name = "state", Order = 3)]
        public string State { get; set; }
        [DataMember(Name = "type", Order = 3)]
        public string StationType { get; set; }

        [DataMember(Name = "has_blackmarket", Order = 3)]
        public bool? BlackMarket { get; set; }
        [DataMember(Name = "has_commodities", Order = 3)]
        public bool? Commodities { get; set; }
        [DataMember(Name = "has_refuel", Order = 3)]
        public bool? HasRefuel { get; set; }
        [DataMember(Name = "has_repair", Order = 3)]
        public bool? HasRepair { get; set; }
        [DataMember(Name = "has_rearm", Order = 3)]
        public bool? HasRearm { get; set; }
        [DataMember(Name = "has_outfitting", Order = 3)]
        public bool? Outfitting { get; set; }
        [DataMember(Name = "has_shipyard", Order = 3)]
        public bool? Shipyard { get; set; }
        [DataMember(Name = "economies", Order = 11)]
        public string[] economies { get; set; }


        public EconomyCollection Economies
        {
            get
            {
                return new EconomyCollection(economies);
            }
        }

        public String RawName { get; set; }
        public ISystem System { get; set; }


        public string SystemName
        {
            get
            {
                return System.Name;
            }
        }

        public PadSize PadSize
        {
            get
            {
                if (MaxLandingPadSize==null) return PadSize.Unknown;
                switch (MaxLandingPadSize.ToUpperInvariant())
                {
                    case "M":
                        return PadSize.Medium;
                    case "L":
                        return PadSize.Large;
                    default:
                        return PadSize.Unknown;
                }
            }
        }


    }

    [DebuggerDisplay("StationProvider {Source.Who,nq}")]
    class EddbProvider : StationProvider
    {
        public EddbProvider(SharedData sharedData, string id) : base(sharedData, id) { }
        private Dictionary<int, System> systems;

        private Source source;
        private List<Station> stations;

        public override IEnumerable<ISystem> Systems
        {
            get { return new List<ISystem>(systems.Values); }
        }

        public async override Task Fetch(bool skipStations = false)
        {
            SendMessage("Fetch: " + Source.Who);
            if (stations != null) return;

            await DoFetch(skipStations);
        }

        private async Task DoFetch(bool skipStations)
        {
            var incrementAllocation = skipStations ? ProgressIncrement : ProgressIncrement / 2.0;
            var fetchingSystems = FetchSystems(incrementAllocation);
            if (!skipStations)
            {
                await FetchStations(incrementAllocation/ 2.0);
            }
            await fetchingSystems;
            SetProgress(incrementAllocation);

            if (!skipStations)
            {
                ProcessStations(incrementAllocation / 2.0);
            }
            SendMessage("Complete: " + Source.Who);
        }

        private async Task FetchSystems(double incrementAllocation)
        {
            var json = await GetJsonFromWeb("systems.json");
            SetProgress(incrementAllocation / 4.0);
            var sys = JsonConvert.DeserializeObject<System[]>(json).ToList();
            SetProgress(incrementAllocation / 4.0);
            systems = new Dictionary<int, System>();
            if (!sys.Any())
            {
                SetProgress(incrementAllocation / 2.0);
                return;
            }
            var increment = incrementAllocation / 2.0 / sys.Count();
            foreach (var system in sys)
            {
                system.RawName = system.Name;
                system.Name = SharedData.CorrectSystemName(system.Name, Source);
                systems.Add(system.Id, system);
                SetProgress(increment);
            }
        }

        private async Task FetchStations(double incrementAllocation)
        {
            var json = await GetJsonFromWeb("stations_lite.json");
            SetProgress(incrementAllocation / 2.0);
            stations = JsonConvert.DeserializeObject<Station[]>(json).ToList();
            SetProgress(incrementAllocation / 2.0);
        }

        private void ProcessStations(double incrementAllocation)
        {
            SendMessage("Process: " + Source.Who);

            if (!stations.Any())
            {
                SetProgress(incrementAllocation);
                return;
            }
            var increment = incrementAllocation / systems.Count(); 
            foreach (var station in stations)
            {
                var system = systems[station.SystemId];
                if (system == null) continue;
                station.RawName = station.Name;
                station.Name = SharedData.CorrectStationName(station.Name, system.Name, Source);
                station.System = system;
                system.Stations.Add(station);
                SetProgress(increment);
            }
        }


        public override IEnumerable<IStation> Stations
        {
            get
            {
                return stations;
            }
        }

        private async Task<string> GetJsonFromWeb(string file)
        {
            using (var webClient = new WebClient())
            {
                SendMessage("Fetching " + Source.Who + " " + file);
                webClient.Headers[HttpRequestHeader.AcceptEncoding] = "gzip, deflate, sdch";
                //var json = webClient.DownloadString("http://eddb.io/archive/v3/" + file);

                string json;
                var uri = new Uri("http://eddb.io/archive/v3/" + file);

                using (var responseStream = await webClient.OpenReadTaskAsync(uri))
                using (var decompressStream = new GZipStream(responseStream, CompressionMode.Decompress))
                using (var reader = new StreamReader(decompressStream))
                {
                    json = reader.ReadToEnd();
                }
                SendMessage("Complete " + Source.Who + " " + file);
                return json;
            }
        }

        //private string GetJsonFromFile()
        //{
        //    using (var r = new StreamReader(@"data\eddbsystems.json"))
        //    {
        //        string json = r.ReadToEnd();
        //        return json;
        //    }
        //}

        //private string GetJsonFromEdsc()
        //{
        //    var request = (HttpWebRequest)WebRequest.Create("http://edstarcoordinator.com/api.asmx/GetSystems");

        //    request.Method = "POST";
        //    request.ContentType = "application/json; charset=utf-8";

        //    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
        //    {
        //        string json = @"{""data"": {""ver"":""2"", ""outputmode"":""2"", ""filter"":{""cr"":""0"", ""date"":""2014-12-00 00:00:00""} } }";

        //        streamWriter.Write(json);
        //        streamWriter.Flush();
        //        streamWriter.Close();
        //    }

        //    var response = (HttpWebResponse)request.GetResponse();

        //    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        //    return responseString;
        //}


        public override Source Source
        {
            get { return source ?? (source = new Source("EDDB", "Starchart_Eddb")); }
        }
    }
}
