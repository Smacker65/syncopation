﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Syncopation.Model.PilotLog
{

    [DebuggerDisplay("PLStation {Name}")]
    public class Station : IStation
    {
        //"ID","Owner","System Name","SystemID","Station Name","Distance","BBS","Black Market","Commodities","Shipyard","Outfitting","Rare Goods","Notes","Removed"
        public Station(string[] parts, Source source, SharedData sharedData)
        {
            for (int index = 0; index < parts.Length; index++)
            {
                parts[index] = parts[index].Trim("\"".ToCharArray());
            }
            SystemName = sharedData.CorrectSystemName(parts[2], source);
            RawName = parts[4];
            Name = sharedData.CorrectStationName(RawName, SystemName, source);
            int distance;
            if (int.TryParse(parts[5], out distance))
            {
                if (distance>0) Distance = distance;
            }
            switch (parts[7])
            {
                case "1":
                    BlackMarket = true;
                    break;
            }
            switch (parts[8])
            {
                case "1":
                    Commodities = true;
                    break;
            }
            switch (parts[9])
            {
                case "1":
                    Shipyard = true;
                    break;
            }
            switch (parts[10])
            {
                case "1":
                    Outfitting = true;
                    break;
            }
            switch (parts[9])
            {
                case "1":
                    PadSize = PadSize.Large;
                    break;
                default:
                    PadSize = PadSize.Unknown;
                    break;
            }
        }

        public string Name { get; private set; }
        public String RawName { get; private set; }
        public string SystemName { get; private set; }

        public ISystem System
        {
            get { return null; }
        }

        public int? Distance { get; private set; }
        public bool? BlackMarket { get; private set; }
        public bool? Commodities { get; private set; }
        public bool? Shipyard { get; private set; }
        public bool? Outfitting { get; private set; }

        public string Allegiance { get { return null; } }
        public string Faction { get { return null; } }
        public string State { get { return null; } }
        public string Government { get { return null; } }
        public EconomyCollection Economies { get { return null; } }


        public PadSize PadSize { get; private set; }
        public string StationType
        {
            get
            {
                switch (PadSize)
                {
                    case PadSize.Large:
                        return "Unknown Starport";
                    case PadSize.Medium:
                    case PadSize.Small:
                        return "Unknown Outpost";
                    default:
                        return "";
                }
            }
        }
    }

    [DebuggerDisplay("StationProvider {Source.Who,nq}")]
    class PilotLogStationProvider : StationProvider
    {
        public PilotLogStationProvider(SharedData sharedData, string id) : base(sharedData, id) { }
        private List<Station> stations;

        public async override Task Fetch(bool skipStations = false)
        {
            //SendMessage("Fetch: " + Source.Who);
            if (Stations != null) return;

            SendMessage("Fetching " + Source.Who + " stations");
            stations = Utils.ReadLines(@"data\PLStations.csv").Select(line => line.Split(','))
               .Where(parts => parts.Length == 14 && !parts[0].StartsWith("\"ID\"") && parts[13] == "\"0\"")
               .Select(parts => new Station(parts, Source, SharedData)).ToList();
            //SendMessage("Complete " + Source.Who + " stations");
            SetProgress(ProgressIncrement);
        }

        public override IEnumerable<IStation> Stations
        {
            get
            {
                return stations;
            }
        }

        private Source source;
        public override Source Source
        {
            get { return source ?? (source = new Source("PilotLog", "PilotLog")); }
        }
    }



}


