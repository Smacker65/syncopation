﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Syncopation.Model.RedWizzard
{

    [DataContract]
    public class Systems
    {
        [DataMember(Name = "ver", Order = 1)]
        public double Ver;
        [DataMember(Name = "date", Order = 2)]
        public DateTime Date;

        [DataMember(Name = "systems", Order = 3)]
        public System[] systems;
    }


    [DebuggerDisplay("RWSystem {Name}")]
    [DataContract]
    public class System : ISystem
    {
        [DataMember(Name = "id", Order = 1)]
        public int Id { get; set; }
        [DataMember(Name = "name", Order = 2)]
        public string Name { get; set; }

        [DataMember(Name = "coord", Order = 3)]
        public double?[] Coords { get; set; }


        public Position Position
        {
            get
            {
                if (!Coords[0].HasValue || !Coords[1].HasValue || !Coords[2].HasValue) return null;
                return new Position(Coords[0], Coords[1], Coords[2]);
            }
        }


        public string RawName { get; set; }
        public string Allegiance { get; private set; }
        public string Faction { get; private set; }
        public string State { get; private set; }
        public string Government { get; private set; }

        public double? Population { get; set; }
        public string PrimaryEconomy { get; set; }


        public bool? RequiresPermit { get { return null; }
            set { }
        }

        public bool DoneWithStations
        {
            get { return false; }
            set {  }
        }

        public int? StationCount
        {
            get { return null; }
        }
    }


    [DebuggerDisplay("StationProvider {Source.Who,nq}")]
    class RedWizzardProvider : SystemProvider
    {
        public RedWizzardProvider(SharedData sharedData, string id) : base(sharedData, id) { }
        private Dictionary<int, System> systems;

        private Source source;

        public override IEnumerable<ISystem> Systems
        {
            get
            {
                return new List<ISystem>(systems.Values);
            }
        }

        public async override Task Fetch(bool skipStations = false)
        {
            SendMessage("Fetch: " + Source.Who);
            if (systems != null) return;

            await DoFetch();
        }

        private async Task DoFetch()
        {
            var fetchingSystems = FetchSystems(ProgressIncrement);
            await fetchingSystems;
            SetProgress(5);

            SendMessage("Complete: " + Source.Who);
        }

        private async Task FetchSystems(double incrementAllocation)
        {
            var json = await GetJsonFromWeb(@"https://raw.githubusercontent.com/SteveHodge/ed-systems/master/tgcsystems.json", "systems");
            SetProgress(incrementAllocation / 4.0);

            var sys = JsonConvert.DeserializeObject<Systems>(json).systems;
            SetProgress(incrementAllocation / 4.0);
            systems = new Dictionary<int, System>();
            if (!sys.Any())
            {
                SetProgress(incrementAllocation / 2.0);
                return;
            }
            var increment = incrementAllocation / 2.0 / sys.Count(); 

            foreach (var system in sys)
            {
                system.RawName = system.Name;
                //system.Name = SharedData.CorrectSystemName(system.Name, Source);
                systems.Add(system.Id, system);
                SetProgress(increment);
            }
        }

        private async Task<string> GetJsonFromWeb(string uriAddress, string what)
        {
            using (var webClient = new WebClient { Encoding = Encoding.UTF8 })
            {
                SendMessage("Fetching " + Source.Who + " " + what);
                var uri = new Uri(uriAddress);
                var json = await webClient.DownloadStringTaskAsync(uri);
                //SendMessage("Complete " + Source.Who + " stations");
                return json;
            }
        }

        public override Source Source
        {
            get { return source ?? (source = new Source("RedWizzard", "RedWizzard")); }
        }
    }
}
