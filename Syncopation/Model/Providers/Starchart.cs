﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Syncopation.Model.Starchart
{

    [DataContract]
    public class Systems
    {
        [DataMember(Name = "systems", Order = 1)]
        public System[] systems;
    }

    [DebuggerDisplay("SCSystemDetail {Allegiance}/{Economy}/{Government}")]
    [DataContract]
    public class SystemDetail
    {
        [DataMember(Name = "population", Order = 1, EmitDefaultValue = false)]
        public double? Population { get; set; }
        [DataMember(Name = "allegiance", Order = 2, EmitDefaultValue = false)]
        public string Allegiance { get; set; }
        [DataMember(Name = "economy", Order = 3, EmitDefaultValue = false)]
        public string Economy { get; set; }
        [DataMember(Name = "government", Order = 4, EmitDefaultValue = false)]
        public string Government { get; set; }
        [DataMember(Name = "requires_permit", Order = 5, EmitDefaultValue = false)]
        public bool RequiresPermit { get; set; }

    }

    [DebuggerDisplay("SCSystem {Name}")]
    [DataContract]
    public class System : ISystem
    {
        public System()
        {
        }

        public System(ISystem system, string commander)
        {
            Name = system.Name;
            RawName = system.RawName;
            Position = system.Position;
            Allegiance = system.Allegiance;
            //Faction = system.Faction;
            //State = system.State;
            Government = system.Government;
            Population = system.Population;
            PrimaryEconomy = system.PrimaryEconomy;
            RequiresPermit = system.RequiresPermit;

            Modified = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ");
            Cmdr = commander;
            Valid = true;
        }

        [DataMember(Name = "system", Order = 1)]
        public string Name { get; set; }

        public string RawName { get; set; }

        [DataMember(Name = "coords", Order = 2)]
        public double[] Coords { get; set; }

        [DataMember(Name = "detail", Order = 3, EmitDefaultValue = false)]
        private SystemDetail Detail { get; set; }

        [DataMember(Name = "modified", Order = 4)]
        public string Modified { get; set; }
        [DataMember(Name = "cmdr", Order = 5)]
        public string Cmdr { get; set; }
        [DataMember(Name = "valid", Order = 6)]
        public bool Valid { get; set; }

        [DataMember(Name = "stations", Order = 7, EmitDefaultValue = false)]
        public Station[] Stations { get; set; }

        public double? Population
        {
            get
            {
                return Detail == null ? null : Detail.Population;
            }
            set
            {
                Detail = Detail ?? new SystemDetail();
                Detail.Population = value;
            }
        }

        public string State
        {
            get { return null; }
        }

        public string Government
        {
            get
            {
                return Detail == null ? null : (Detail.Government == "Anarchy") ? null : Detail.Government;
            }
            set
            {
                Detail = Detail ?? new SystemDetail();
                Detail.Government = (value=="Anarchy") ? null :value;
            }
        }
        public string PrimaryEconomy
        {
            get
            {
                return Detail == null ? null : Detail.Economy;
            }
            set
            {
                Detail = Detail ?? new SystemDetail();
                Detail.Economy = value;
            }
        }
        public string Allegiance
        {
            get
            {
                return Detail == null ? null : (Detail.Allegiance == "None") ? null : Detail.Allegiance;
            }
            set
            {
                Detail = Detail ?? new SystemDetail();
                Detail.Allegiance = (value == "None") ? null : value;
            }
        }

        public string Faction
        {
            get { return null; }
        }

        public bool? RequiresPermit
        {
            get
            {
                return Detail != null && Detail.RequiresPermit;
            }
            set
            {
                Detail = Detail ?? new SystemDetail();
                Detail.RequiresPermit = value ?? false;
            }
        }

        public bool DoneWithStations { get; set; }

        public int? StationCount
        {
            get
            {
                if (Stations == null) return 0;
                return Stations.Count();
            }
        }

        public void UpdateStation(StationMatch station)
        {
            var s = (Stations ?? new Station[] { }).Where(st => st.Inferred != true && !String.Equals(st.Name, station.Name, StringComparison.InvariantCultureIgnoreCase)).ToList();
            s.Add(new Station(station, this));
            Stations = s.ToArray();
        }

        public Position Position
        {
            get
            {
                return new Position(Coords[0], Coords[1], Coords[2]);
            }
            set 
            {
                Coords = (value == null) ? null : new[] { value.x, value.y, value.z };
            }
        }

        internal bool HasStation(string name)
        {
            return Stations != null && Stations.Any(s => String.Equals(s.Name, name, StringComparison.InvariantCultureIgnoreCase));
        }

        public void UpdateSystem(SystemMatch systemMatch)
        {
            Name = systemMatch.Name;
            Allegiance = systemMatch.Allegiance;
            Government = systemMatch.Government;
            Population = systemMatch.Population;
            PrimaryEconomy = systemMatch.PrimaryEconomy;
            //Faction = systemMatch.Faction;
            //State = systemMatch.State;
            //RequiresPermit = systemMatch.RequiresPermit;
        }
    }

    [DebuggerDisplay("SCStation {Name}")]
    [DataContract]
    public class Station : IStation
    {
        public Station()
        {
        }

        public Station(StationMatch station, ISystem system)
        {
            Name = station.Name;
            RawName = station.RawName;
            BlackMarket = station.BlackMarket;
            StationType = station.StationType;
            distance = station.Dist;
            Faction = station.Faction;
            Government = station.Government;
            Allegiance = station.Allegiance;
            Shipyard = station.Shipyard;
            Outfitting = station.Outfitting;
            Commodities = station.Commodities;
            Economies = station.Economies;

            System = system;
        }

        [DataMember(Name = "station", Order = 1)]
        public string Name { get; set; }
        [DataMember(Name = "inferred", Order = 2, EmitDefaultValue = false)]
        public bool? Inferred { get; set; }
        [DataMember(Name = "black_market", Order = 3, EmitDefaultValue = false)]
        public bool? BlackMarket { get; set; }
        [DataMember(Name = "type", Order = 4, EmitDefaultValue = false)]
        public string StationType { get; set; }
        [DataMember(Name = "distance", Order = 5, EmitDefaultValue = false)]
        public double? distance { get; set; }
        [DataMember(Name = "faction", Order = 6, EmitDefaultValue = false)]
        public string Faction { get; set; }
        [DataMember(Name = "government", Order = 7, EmitDefaultValue = false)]
        public string Government { get; set; }
        [DataMember(Name = "allegiance", Order = 8, EmitDefaultValue = false)]
        public string Allegiance { get; set; }
        [DataMember(Name = "economy", Order = 9, EmitDefaultValue = false)]
        public string[] economies { get; set; }
        [DataMember(Name = "shipyard", Order = 10, EmitDefaultValue = false)]
        public bool? Shipyard { get; set; }
        [DataMember(Name = "outfitting", Order = 11, EmitDefaultValue = false)]
        public bool? Outfitting { get; set; }
        [DataMember(Name = "commodities_market", Order = 12, EmitDefaultValue = false)]
        public bool? Commodities { get; set; }

        public string State { get { return null; } }

        public EconomyCollection Economies
        {
            get
            {
                return new EconomyCollection(economies);
            }
            set
            {
                economies = (value == null) ? null : value.Economies;      
            }
        }

        public String RawName { get; set; }

        public ISystem System { get; set; }

        public string SystemName
        {
            get
            {
                return System.Name;
            }
        }

        public int? Distance
        {
            get
            {
                if (distance == null) return null;
                return Convert.ToInt32(distance);
            }
        }

        public PadSize PadSize
        {
            get
            {
                if (StationType == null) return PadSize.Unknown;
                if (StationType.ToUpper().Contains("OUTPOST")) return PadSize.Medium;
                if (StationType.ToUpper().Contains("STARPORT")) return PadSize.Large;
                return PadSize.Unknown;
            }
        }


    }

    [DebuggerDisplay("StationProvider {Source.Who,nq}")]
    public class StarChartProvider : StationProvider
    {
        public StarChartProvider(SharedData sharedData, string id) : base(sharedData, id) { }

        public const string BaseWebsite = "http://starchart.club"; // ("http://localhost:6968");

        public override IEnumerable<ISystem> Systems
        {
            get { return new List<ISystem>(systems); }
        }

        private System[] systems;
        private List<Station> stations;

        public async override Task Fetch(bool skipStations = false)
        {
            //SendMessage("Fetch: " + Source.Who);
            if (systems != null) return;
            var json = await FetchJson("Systems + Stations");
            SetProgress(ProgressIncrement / 2.0);
            systems = JsonConvert.DeserializeObject<Systems>(json).systems;

            var incrementAllocation = skipStations ? ProgressIncrement/2.0 : ProgressIncrement/4.0;
            ProcessSystems(incrementAllocation);
            if (!skipStations)
            {
                ProcessStations(incrementAllocation);
            }
            SendMessage("Complete: " + Source.Who);
        }

        private void ProcessSystems(double incrementAllocation)
        {
            SendMessage("Process: " + Source.Who + " systems");
            if (!systems.Any())
            {
                SetProgress(incrementAllocation);
                return;
            }
            var increment = incrementAllocation / systems.Count(); 
            foreach (var system in systems)
            {
                system.RawName = system.Name;
                SetProgress(increment);
            }
        }

        private void ProcessStations(double incrementAllocation)
        {
            SendMessage("Process: " + Source.Who + " stations");

            stations = new List<Station>();
            var inhabitedSystems = systems.Where(s => s.Stations != null).ToList();
            if (!inhabitedSystems.Any())
            {
                SetProgress(incrementAllocation);
                return;
            }
            var increment = incrementAllocation/inhabitedSystems.Count();
            foreach (var system in inhabitedSystems)
            {
                foreach (var station in system.Stations.Where(s => s.Inferred != true))
                {
                    station.RawName = station.Name;
                    station.Name = SharedData.CorrectStationName(station.Name, system.Name, Source);
                    if (stations.Any(s => s.Name == station.Name))
                    {
                        station.Name = station.RawName;
                    }
                    station.System = system;
                    stations.Add(station);
                }
                SetProgress(increment);
            }
        }

        public override IEnumerable<IStation> Stations
        {
            get
            {
                return stations;
            }
        }

        private async Task<string> FetchJson(string what)
        {
            using (var webClient = new WebClient { Encoding = Encoding.UTF8 })
            {
                SendMessage("Fetching " + Source.Who + " " + what);
                var uri = new Uri(BaseWebsite + "/elite.json");
                var json = await webClient.DownloadStringTaskAsync(uri);
                //SendMessage("Complete " + Source.Who + " stations");
                return json;
            }
        }

        private Source source;
        public override Source Source
        {
            get { return source ?? (source = new Source("Starchart", "Starchart_Eddb")); }
        }
    }

}
