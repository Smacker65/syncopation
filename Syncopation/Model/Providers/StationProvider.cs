﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Syncopation.Model
{
    public enum PadSize
    {
        Small,
        Medium,
        Large,
        Unknown,
    }

    public interface IStation : IEntity
    {
        string SystemName { get; }
        ISystem System { get; }

        int? Distance { get; }
        bool? BlackMarket { get; }
        bool? Commodities { get; }
        bool? Shipyard { get; }
        bool? Outfitting { get; }
        PadSize PadSize { get; }
        string StationType { get; }

        EconomyCollection Economies { get; }
    }

    public class EconomyCollection : IEquatable<EconomyCollection>
    {
        public EconomyCollection(string[] economies)
        {
            Economies = economies;
        }

        public string[] Economies { get; private set; }

        public bool Equals(EconomyCollection other)
        {
            return Economies.SequenceEqual(other.Economies);
        }

        public override string ToString()
        {
            return Economies == null ? "" : string.Join(",", Economies);
        }
    }



    public interface IStationProvider : ISystemProvider
    {
        IEnumerable<IStation> Stations { get; }
    }

    public abstract class StationProvider : SystemProvider, IStationProvider
    {

        protected StationProvider(SharedData sharedData, string id)
            : base(sharedData, id)
        {
        }

        public abstract IEnumerable<IStation> Stations { get; }
    }

}
