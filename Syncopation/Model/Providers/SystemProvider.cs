﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Syncopation.Model
{

    public interface IEntity
    {
        string Name { get;  }
        string RawName { get; }
        string Allegiance { get; }
        string Faction { get; }
        string State { get; }
        string Government { get; }
    }

    public interface ISystem : IEntity
    {
        Position Position { get; }
        double? Population { get; set; }
        string PrimaryEconomy { get; set; }
        bool? RequiresPermit { get; set; }

        int? StationCount { get; }
    }


    [DebuggerDisplay("Source {Who,nq} ({Type,nq})")]
    public class Source : IEquatable<Source>
    {
        public Source(string who, string type)
        {
            Who = who;
            Type = type;
        }

        public string Who { get; private set; }
        public string Type { get; private set; }
        public bool Equals(Source other)
        {
            return Who == other.Who;
        }

        public override int GetHashCode()
        {
            return Who.GetHashCode();
        }
    }

    public interface ISystemProvider
    {
        Task Fetch(bool skipStations = false);
        IEnumerable<ISystem> Systems { get; }
        Source Source { get; }
        int ProcessOrder { get; set; }
        event EventHandler<MessageEventArgs> Message;
        event EventHandler<FineProgressChangedEventArgs> Progress;
        double ProgressIncrement { set; }
    }

    public abstract class SystemProvider : ISystemProvider
    {
        private readonly string id;
        protected readonly SharedData SharedData;

        protected SystemProvider(SharedData sharedData, string id)
        {
            this.id = id;
            SharedData = sharedData;
        }

        public abstract Task Fetch(bool skipStations = false);
        public virtual IEnumerable<ISystem> Systems { get { return null; } }
        public abstract Source Source { get; }
        public int ProcessOrder { get; set; }
        public double ProgressIncrement { set; protected get; }

        public event EventHandler<MessageEventArgs> Message = delegate { };
        public event EventHandler<FineProgressChangedEventArgs> Progress = delegate { };

        protected void SendMessage(string message)
        {
            Message(this, new MessageEventArgs(message, id));
            Logger.Message(message);
        }
        protected void SetProgress(double progress)
        {
            Progress(this, new FineProgressChangedEventArgs(progress, id));
        }
    }

}
