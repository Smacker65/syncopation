﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Syncopation.Model.TradeDangerous
{

    [DebuggerDisplay("TDSystem {Name}")]
    public class Station : IStation
    {

        //unq:name@System.system_id,unq:name,ls_from_star,blackmarket,max_pad_size
        public Station(IList<string> parts, Source source, SharedData sharedData)
        {
            for (var index = 0; index < parts.Count; index++)
            {
                parts[index] = parts[index].Trim("'".ToCharArray()).Replace("''", "'");
            }
            SystemName = parts[0];
            RawName = parts[1];
            Name = sharedData.CorrectStationName(RawName, SystemName, source);
            int distance;
            if (int.TryParse(parts[2], out distance))
            {
                if (distance>0) Distance = distance;
            }
            switch (parts[3])
            {
                case "Y":
                    BlackMarket = true;
                    break;
                case "N":
                    BlackMarket = false;
                    break;
            }
            switch (parts[4])
            {
                case "S":
                case "M":
                    PadSize = PadSize.Medium;
                    break;
                case "L":
                    PadSize = PadSize.Large;
                    break;
                default:
                    PadSize = PadSize.Unknown;
                    break;
            }
        }

        public string Name { get; private set; }
        public String RawName { get; private set; }
        public string SystemName { get; private set; }

        public ISystem System
        {
            get { return null; }
        }

        public int? Distance { get; private set; }
        public bool? BlackMarket { get; private set; }
        public bool? Commodities { get; private set; }
        public bool? Shipyard { get; private set; }
        public bool? Outfitting { get; private set; }
        public PadSize PadSize { get; private set; }

        public string Allegiance { get { return null; } }
        public string Faction { get { return null; } }
        public string State { get { return null; } }
        public string Government { get { return null; } }
        public EconomyCollection Economies { get { return null; } }

        public string StationType
        {
            get
            {
                switch (PadSize)
                {
                    case PadSize.Large:
                        return "Unknown Starport";
                    case PadSize.Medium:
                    case PadSize.Small:
                        return "Unknown Outpost";
                    default:
                        return "";
                }
            }
        }
       
    }

    [DebuggerDisplay("StationProvider {Source.Who,nq}")]
    class TradeDangerousStationProvider : StationProvider
    {
        private readonly string who;
        private readonly string pathToCsv;
        private Source source;
        private List<Station> stations;

        public TradeDangerousStationProvider(SharedData sharedData, string id, string who, string pathToCsv)
            : base(sharedData, id)
        {
            this.who = who;
            this.pathToCsv = pathToCsv;
        }

        public async override Task Fetch(bool skipStations = false)
        {
            //SendMessage("Fetch: " + Source.Who);
            if (stations != null) return;

            var lines = await ReadLines(pathToCsv);
            SetProgress(ProgressIncrement / 2.0);

            var filtered = lines.Select(line => line.Split(',')).Where(p => p.Length == 5 && p[0].StartsWith("'"));
            stations = filtered.Select(p => new Station(p, Source, SharedData)).ToList();
            SetProgress(ProgressIncrement / 2.0);
        }

        public override IEnumerable<IStation> Stations
        {
            get
            {
                return stations;
            }
        }

        private async Task<IEnumerable<string>> ReadLines(string uri)
        {
            string csv;
            using (var webClient = new WebClient())
            {
                SendMessage("Fetching " + who + " stations");
                csv = await webClient.DownloadStringTaskAsync(uri);
            }

            var lines = csv.Split('\n');
            //SendMessage("Complete " + who + " stations");
            return lines;
        }

        public override Source Source
        {
            get { return source ?? (source = new Source(who, "TradeDangerous")); }
        }

    }



}


