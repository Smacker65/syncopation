﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Syncopation.Model
{
    public class SharedData
    {

        public List<string> SystemExclusions { get; private set; }
        public List<string> SystemUpperExclusions { get; private set; }
        public List<string> SystemStationExclusions { get; private set; }
        private List<string> StationPreFixes { get; set; }
        private List<string> StationPostFixes { get; set; }
        private List<string> StationPrefixExclusions { get; set; }
        private List<string> StationPostfixExclusions { get; set; }
        private Dictionary<string, string> StationRenames { get; set; }
        private Dictionary<string, string> SystemRenames { get; set; }
        public List<string> Corrections { get; private set; }
        public List<string> CaseCorrections { get; private set; }
        public Dictionary<string, Mismatch> Mismatches { get; private set; }
        public Dictionary<string, ISystem> SystemNames { get; private set; }
        public Dictionary<Position, ISystem> SystemPositions { get; private set; }

        public SharedData()
        {
            Init();
        }

        private void Init()
        {
            SystemExclusions = File.ReadAllLines(@"data\SystemExclusions.txt").Where(s => s.Length > 0).Select(s => s.ToUpperInvariant()).OrderByDescending(s => s.Length).ThenBy(s => s).ToList();
            SystemUpperExclusions = File.ReadAllLines(@"data\SystemUpperExclusions.txt").Where(s => s.Length > 0).Select(s => s.ToUpperInvariant()).OrderByDescending(s => s.Length).ThenBy(s => s).ToList();
            SystemStationExclusions = File.ReadAllLines(@"data\SystemStationExclusions.txt").Where(s => s.Length > 0).Select(s => s.ToUpperInvariant()).OrderByDescending(s => s.Length).ThenBy(s => s).ToList();
            StationPreFixes = File.ReadAllLines(@"data\StationPrefixes.txt").Where(s => s.Length > 0).OrderByDescending(s => s.Length).ThenBy(s => s).ToList();
            // StationPreFixes.Clear();

            StationPostFixes = File.ReadAllLines(@"data\StationPostfixes.txt").Where(s => s.Length > 0).OrderByDescending(s => s.Length).ThenBy(s => s).ToList();
            StationPostfixExclusions = File.ReadAllLines(@"data\StationPostfixExclusions.txt").Where(s => s.Length > 0).OrderByDescending(s => s.Length).ThenBy(s => s).ToList();
            StationPrefixExclusions = File.ReadAllLines(@"data\StationPrefixExclusions.txt").Where(s=>s.Length>0).OrderByDescending(s => s.Length).ThenBy(s => s).ToList();
            StationRenames = File.ReadAllLines(@"data\StationRenames.txt").Select(r => r.Split(',')).Where(r => r.Length == 2).ToDictionary(r => r[0].ToUpperInvariant(), r => r[1]);
            SystemRenames = File.ReadAllLines(@"data\SystemRenames.txt").Select(r => r.Split(',')).Where(r => r.Length == 2).ToDictionary(r => r[0].ToUpperInvariant(), r => r[1]);
            Corrections = new List<string>();
            CaseCorrections = new List<string>();
            Mismatches = new Dictionary<string, Mismatch>();
            SystemNames = new Dictionary<string, ISystem>();
            SystemPositions = new Dictionary<Position, ISystem>();
        }

        public void Clear()
        {
            Corrections.Clear();
            CaseCorrections.Clear();
            Mismatches.Clear();
            SystemNames.Clear();
        }



        public void ScanSystems(ISystemProvider provider)
        {
            foreach (var system in provider.Systems)
            {
                var systemKey = system.Name.ToUpperInvariant();
                if (!SystemNames.ContainsKey(systemKey))
                {
                    SystemNames.Add(systemKey, system);
                }

                if (!SystemPositions.ContainsKey(system.Position))
                {
                    SystemPositions.Add(system.Position, system);
                }
            }

        }

        public string CorrectSystemName(string rawName, Source source)
        {
            string name = rawName.Trim();
            name = ApplySystemRenames(name, source);
            return name;
        }

        public string CorrectStationName(string rawName, string systemName, Source source)
        {
            string name = rawName.Trim();
            name = ApplyStationRenames(name, systemName, source);
            name = CorrectStandardNames(name, systemName, source);
            return name;
        }

        private string ApplySystemRenames(string rawName, Source source)
        {
            if (!SystemRenames.ContainsKey(rawName.ToUpperInvariant())) return rawName;
            var newName = SystemRenames[rawName.ToUpperInvariant()];
            if (newName == rawName) return rawName;
            return CorrectIfRequired(rawName, newName, () => string.Format("Renaming System       {0} to {1} [{2}]", rawName, newName, source.Who));
        }

        private string ApplyStationRenames(string rawName, string systemName, Source source)
        {
            if (!StationRenames.ContainsKey(rawName.ToUpperInvariant())) return rawName;
            var newName = StationRenames[rawName.ToUpperInvariant()];
            return CorrectIfRequired(rawName, newName, () => string.Format("Renaming Station       {0} to {1} in {2} [{3}]", rawName, newName, systemName, source.Who));
        }

        private string CorrectIfRequired(string rawName, string newName, Func<string> line)
        {
            if (newName == rawName) return rawName;
            if (String.Equals(newName, rawName, StringComparison.OrdinalIgnoreCase))
            {
                CaseCorrections.Add(line());
            }
            else
            {
                Corrections.Add(line());
            }
            return newName;
        }

        private string CorrectStandardNames(string rawName, string systemName, Source source)
        {
            if (StationPostfixExclusions.Any(exclusion => rawName.ToUpperInvariant().EndsWith(exclusion.ToUpperInvariant()))
            || StationPrefixExclusions.Any(exclusion => rawName.ToUpperInvariant().StartsWith(exclusion.ToUpperInvariant())))
            {
                return rawName;
            }

            var fixedName = InsertMissingSpace(rawName, systemName, source);
            var parts = fixedName.Split(' ');
            if (parts.Length<2) return fixedName;

            var postfix = parts.LastOrDefault();
            if (postfix==null) return fixedName;
            var prefix = fixedName.Substring(0, fixedName.Length - postfix.Length - 1);
            prefix = ReplaceSimilar(2, prefix, StationPreFixes).Value;
            postfix = ReplaceSimilar(2, postfix, StationPostFixes).Value;

            fixedName = prefix + " " + postfix;

            if (fixedName!=rawName)
            {
                CorrectIfRequired(rawName, fixedName, () => string.Format("Standard Name fixed    {0} => {1} in {2} [{3}]", rawName, fixedName, systemName, source.Who));
            }
            return fixedName;
        }

        private class Replacement
        {
            public Replacement(string value, bool found)
            {
                Value = value;
                Found = found;
            }

            public string Value { get; private set; }
            public bool Found { get; private set; }
        }

        private Replacement ReplaceSimilar(int maxDistance, string toFix, List<string> corrections)
        {
            foreach (var fix in corrections.Where(fix => String.Equals(toFix, fix, StringComparison.InvariantCultureIgnoreCase)))
            {
                return new Replacement(fix, true);
            }
            foreach (var fix in corrections)
            {
                var dist = Utils.LevenshteinDistance(fix, toFix);
                if (dist < maxDistance)
                {
                    return new Replacement(fix, true);
                }
            }
            return new Replacement(toFix, false);
        }

        private string InsertMissingSpace(string rawName, string systemName, Source source)
        {
            var rawUpper = rawName.ToUpperInvariant();
            foreach (var postFix in StationPostFixes)
            {
                if (String.Equals(rawName, postFix, StringComparison.InvariantCultureIgnoreCase))
                {
                    // Not a postfix
                    return rawName;
                }

                if (rawUpper.EndsWith(postFix.ToUpperInvariant()))
                {
                    var stripped = rawName.Substring(0, rawName.Length - postFix.Length);

                    var prefix = ReplaceSimilar(3, stripped.Trim(), StationPreFixes);
                    if (!prefix.Found)
                    {
                        return rawName;
                    }

                    if (!stripped.EndsWith(" "))
                    {
                        Corrections.Add(string.Format("Postfix Spacing {0} => {1} {2} in {3} [{4}]", rawName, prefix.Value, postFix, systemName, source.Who));
                    }
                    else
                    {
                        if (rawName.EndsWith(postFix))
                        {
                            // Exact match
                            return rawName;
                        }
                        CaseCorrections.Add(string.Format("Postfix Case    {0} => {1} {2} in {3} [{4}]", rawName, prefix.Value, postFix, systemName, source.Who));
                    }
                    return prefix.Value + " " + postFix;
                }
            }

            foreach (var prefix in StationPreFixes)
            {
                if (String.Equals(rawName, prefix, StringComparison.InvariantCultureIgnoreCase))
                {
                    // Not a prefix
                    return rawName;
                }

                if (rawUpper.StartsWith(prefix.ToUpperInvariant()))
                {
                    var stripped = rawName.Substring(prefix.Length);

                    var postfix = ReplaceSimilar(3, stripped.Trim(), StationPostFixes);
                    if (!postfix.Found)
                    {
                        return rawName;
                    }

                    if (!stripped.StartsWith(" "))
                    {
                        Corrections.Add(string.Format("Prefix Spacing {0} => {1} {2} in {3} [{4}]", rawName, prefix, postfix.Value, systemName, source.Who));
                    }
                    else
                    {
                        if (rawName.EndsWith(prefix))
                        {
                            // Exact match
                            return rawName;
                        }
                        CaseCorrections.Add(string.Format("Prefix Case    {0} => {1} {2} in {3} [{4}]", rawName, prefix, postfix.Value, systemName, source.Who));
                    }
                    return prefix + " " + postfix.Value;
                }
            }

            return rawName;
        }

        public void AddMismatch(string systemName, string stationName, string reason, string reasonMessage, string currentValue, Source currentSource, string newValue, Source newSource)
        {
            var key = (systemName + "/" + stationName + "/" + reason).ToUpper();
            if (!Mismatches.ContainsKey(key))
            {
                Mismatches.Add(key, new Mismatch(systemName, stationName, reason, reasonMessage, currentValue, currentSource));
            }
            Mismatches[key].AddDataPoint(newValue, newSource);
        }

        public void AddMismatch(string systemName, string reason, string reasonMessage, string currentValue, Source currentSource, string newValue, Source newSource)
        {
            var key = (systemName + "/" + reason).ToUpper();
            if (!Mismatches.ContainsKey(key))
            {
                Mismatches.Add(key, new Mismatch(systemName, null, reason, reasonMessage, currentValue, currentSource));
            }
            Mismatches[key].AddDataPoint(newValue, newSource);
        }


        public class GeneratedStation
        {
            public GeneratedStation(string prefix, string postfix)
            {
                Prefix = prefix;
                Postfix = postfix;
            }

            public string Prefix { get; set; }
            public string Postfix { get; set; }
        }

        public GeneratedStation GetPrefix(string name)
        {
            var parts = name.Split(' ');
            if (parts.Count() != 2) return null;
            var postfix = parts.LastOrDefault();
            if (postfix==null) return null;
            if (StationPostfixExclusions.Contains(postfix)) return null;
            if (!StationPostFixes.Contains(postfix)) return null;
            return new GeneratedStation( name.Substring(0, name.Length - postfix.Length - 1), postfix);
        }
    }
}
