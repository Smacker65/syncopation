﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Syncopation.Model.Eddb;
using Syncopation.Model.RedWizzard;
using Syncopation.Model.Starchart;
using Syncopation.ViewModel;

namespace Syncopation.Model
{
    public class StarChartUtilities
    {
        private readonly SharedData sharedData;
        private string id;
        private double currentProgress;

        public StarChartUtilities(SharedData sharedData)
        {
            this.sharedData = sharedData;
        }

        public async void Move(string name, Position newPos, string callerId)
        {
            id = callerId;
            SendMessage("Start");

            var starChartProvider = new StarChartProvider(sharedData, callerId);

            starChartProvider.Message += (sender, e) => Message(sender, e);
            starChartProvider.Progress += (sender, e) => ProgressChanged(sender, e);

            await starChartProvider.Fetch(true);

            SendMessage("ScanSystems");
            sharedData.ScanSystems(starChartProvider);

            var edit = new EditRequest("http://starchart.club/api/edit");

            var currentSystem = starChartProvider.Systems.FirstOrDefault(s => s.Name == name) as Starchart.System;
            if (currentSystem == null)
            {
                SendMessage("Move failed " + name + " not found");
                return;
            }
            SendMessage("Moving " + currentSystem.Name + " to " + newPos);
            edit.MoveSystem(currentSystem, newPos, new Source(callerId, "Manual"));

            edit.Execute();

            SendMessage("Done");
        }

        public async void Rename(Dictionary<string, string> renames , string callerId)
        {
            id = callerId;
            SendMessage("Start");

            var starChartProvider = new StarChartProvider(sharedData, callerId);

            starChartProvider.Message += (sender, e) => Message(sender, e);
            starChartProvider.Progress += (sender, e) => ProgressChanged(sender, e);

            await starChartProvider.Fetch(true);

            SendMessage("ScanSystems");
            sharedData.ScanSystems(starChartProvider);

            var edit = new EditRequest("http://starchart.club/api/edit");
            foreach (var rename in renames)
            {
                SendMessage("Renaming " + rename.Key + " to " + rename.Value);
                var currentSystem = starChartProvider.Systems.FirstOrDefault(s => s.Name == rename.Key) as Starchart.System;
                if (currentSystem == null)
                {
                    SendMessage("Rename failed " + rename.Key + " not found");
                    continue;
                }
                currentSystem.Name = rename.Value;
                edit.RenameSystem(currentSystem);
            }
            edit.Execute();

            SendMessage("Done");
        }

        public event EventHandler<FineProgressChangedEventArgs> ProgressChanged = delegate { };

        private void OnProgressChanged(double progress)
        {
            currentProgress += progress;
            ProgressChanged(this, new FineProgressChangedEventArgs(progress, id));
        }

        public event EventHandler<MessageEventArgs> Message = delegate { };

        private void SendMessage(string message)
        {
            Message(this, new MessageEventArgs(message, id));
            Logger.Message(message);
        }

    }
}
