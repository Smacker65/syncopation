using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Syncopation.Model
{
    [DebuggerDisplay("StationMatch {SystemName} {Name} {Sources}")]
    public class StationMatch : BaseMatch
    {
        private readonly List<IMatchValue<IStation>> values;

        public StationMatch(IStation station, Source source)
        {
            values = new List<IMatchValue<IStation>>
            {
                new MatchValue<string, IStation>("Name", "NAME", "Names do not match",
                    station, source,
                    s => s.Name,
                    String.IsNullOrEmpty,
                    null,
                    false),
                new MatchValue<int?, IStation>("Dist", "DIST", "Distances do not match",
                    station, source,
                    s => s.Distance,
                    s => !s.HasValue || s <= 0,
                    DistancesSignificant),
                new MatchValue<bool?, IStation>("BlackMarket", "BMKT", "BlackMarket flags do not match",
                    station, source,
                    s => s.BlackMarket),
                new MatchValue<bool?, IStation>("Shipyard", "SHPY", "Shipyard flags do not match",
                    station, source,
                    s => s.Shipyard),
                new MatchValue<bool?, IStation>("Commodities", "COMM", "Commodities flags do not match",
                    station, source,
                    s => s.Commodities),
                new MatchValue<bool?, IStation>("Outfitting", "OUTF", "Outfitting flags do not match",
                    station, source,
                    s => s.Outfitting),
                new MatchValue<string, IStation>("StationType", "TYPE", "StationTypes do not match",
                    station, source,
                    s => s.StationType,
                    String.IsNullOrEmpty,
                    (v1, v2) => Class(v1) != Class(v2)),
                new MatchValue<string, IStation>("Faction", "FACT", "Factions do not match",
                    station, source,
                    s => s.Faction,
                    s => String.IsNullOrEmpty(s) || s == "-"),
                new MatchValue<string, IStation>("Government", "GOVRN", "Governments do not match",
                    station, source,
                    s => s.Government,
                    String.IsNullOrEmpty),
                new MatchValue<string, IStation>("Allegiance", "ALLEG", "Allegiances do not match",
                    station, source,
                    s => s.Allegiance,
                    String.IsNullOrEmpty),
                new MatchValue<EconomyCollection, IStation>("Economies", "ECON", "Economies do not match",
                    station, source,
                    s => s.Economies,
                    s => s==null || s.Economies==null || s.Economies.Length==0,
                    (v1, v2) => !v1.Equals(v2)),
            };

            RawName = station.RawName;
            SystemName = station.SystemName;

            Sources = new List<Source> { source };
        }

        private string Class(string stationType)
        {
            if (String.IsNullOrEmpty(stationType)) return null;
            return stationType.ToUpperInvariant().Contains("OUTPOST") ? "Outpost" : "Starport";
        }

        private bool DistancesSignificant(int? d1, int? d2)
        {
            if (!d2.HasValue || d2 <= 0) return false;
            if (!d1.HasValue || d1 <= 0) return true;
            var diff = Math.Abs(d2.Value - d1.Value);
            return diff > 10 && (double)diff / d1.Value > 0.05;
        }

        public string SystemName { get; set; }

        public string Name { get { return GetField<string>("Name"); } }

        public int? Dist { get { return GetField<int?>("Dist"); } }
        public bool? BlackMarket { get { return GetField<bool?>("BlackMarket"); } }
        public bool? Shipyard { get { return GetField<bool?>("Shipyard"); } }
        public bool? Commodities { get { return GetField<bool?>("Commodities"); } }
        public bool? Outfitting { get { return GetField<bool?>("Outfitting"); } }
        public string StationType { get { return GetField<string>("StationType"); } }

        public string Faction { get { return GetField<string>("Faction"); } }
        public string Government { get { return GetField<string>("Government"); } }
        public string Allegiance { get { return GetField<string>("Allegiance"); } }
        public EconomyCollection Economies { get { return GetField<EconomyCollection>("Economies"); } }


        private T GetField<T>(string fieldName)
        {
            var field = values.OfType<MatchValue<T, IStation>>().FirstOrDefault(v => v.Name == fieldName);
            return field == null ? default(T) : field.Value;
        }

        public void Update(IStation entity, Source source, SharedData sharedData)
        {
            Sources.Add(source);

            foreach (var value in values)
            {
                value.Update(entity, source, sharedData);
            }
        }


        public void UpdateByConsensus(string reasonCode, ValueWithSources consensus)
        {
            var value = values.FirstOrDefault(v => v.Code == reasonCode);
            if (value == null) return;
            value.Set(consensus.Value, consensus.Sources);
            byConsensus = true;
        }

        
        public override string MatchLine
        {
            get
            {
                var dist = values.FirstOrDefault(v => v.Name == "Dist");
                var bm = values.FirstOrDefault(v => v.Name == "BlackMarket");
                var econ = values.FirstOrDefault(v => v.Name == "Economies");
                var type = values.FirstOrDefault(v => v.Name == "StationType");
                return string.Join("\t", SystemName, Name, dist.ValueAsString, bm.ValueAsString, type.ValueAsString, econ.ValueAsString, SourceForDisplay);
            }
        }

        public List<string> Updates(string baseProvider)
        {
            var changes = new List<string>();
            if (Sources.All(s => s.Who != baseProvider))
            {
                changes.Add(string.Join("\t", "ADDSTN", SystemName, Name, "", SourceForDisplay));
            }
            else
            {
                foreach (var value in values)
                {
                    var newlySeen = value.Sources.All(s => s.Who != baseProvider);
                    if (value.Name == "Name")
                    {
                        if (newlySeen || Name != RawName)
                        {
                            changes.Add(string.Join("\t", "RENA", SystemName, value.ValueAsString, "<== " + RawName, value.SourcesForDisplay));
                        }
                    }
                    else if (newlySeen)
                    {
                        changes.Add(string.Join("\t", value.Code, SystemName, Name, value.ValueAsString, value.SourcesForDisplay));
                    }
                }
            }
            return changes;
        }

    }
    
}