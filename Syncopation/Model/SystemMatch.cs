using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Syncopation.Model
{
    public abstract class BaseMatch
    {
        protected bool byConsensus;

        public string RawName { get; protected set; }
        public List<Source> Sources { get; protected set; }


        public abstract string MatchLine { get; }

        protected string SourceForDisplay
        {
            get
            {
                if (Sources == null || Sources.Count == 0) return "Unknown";
                if (Sources.Count == 1) return Sources[0].Who;
                return string.Join("+", Sources.Select(s => s.Who)) + (byConsensus ? " (By consensus)" : "");
            }
        }

    }

    [DebuggerDisplay("SystemMatch {Name} {Sources}")]
    public class SystemMatch : BaseMatch
    {
        private readonly List<IMatchValue<ISystem>> values;
        public SystemMatch(ISystem system, Source source)
        {
            values = new List<IMatchValue<ISystem>>
            {
                new MatchValue<string, ISystem>("Name", "NAME", "Names do not match",
                    system, source,
                    s => s.Name,
                    String.IsNullOrEmpty,
                    null,
                    false),
                //new MatchValue<string, ISystem>("Faction", "FACT", "Factions do not match",
                //    system, source,
                //    s => s.Faction,
                //    s => String.IsNullOrEmpty(s) || s == "-"),
                new MatchValue<string, ISystem>("Government", "GOVRN", "Governments do not match",
                    system, source,
                    s => s.Government,
                    s => String.IsNullOrEmpty(s) || s=="None" || s=="Anarchy"),
                new MatchValue<string, ISystem>("Allegiance", "ALLEG", "Allegiances do not match",
                    system, source,
                    s => s.Allegiance,
                    s => String.IsNullOrEmpty(s) || s=="None"),
                new MatchValue<string, ISystem>("PrimaryEconomy", "PECON", "Primary Economies do not match",
                    system, source,
                    s => s.PrimaryEconomy,
                    s => String.IsNullOrEmpty(s) || s=="None"),
                new MatchValue<double?, ISystem>("Population", "POPS", "Populations do not match",
                    system, source,
                    s => s.Population,
                    s => !s.HasValue || s.Value<1.0),
                 new MatchValue<double?, ISystem>("StationCount", "#STATIONS", "Station Counts do not match",
                    system, source,
                    s => s.StationCount),
               new MatchValue<bool?, ISystem>("RequiresPermit", "PERM", "RequiresPermit flags do not match",
                    system, source,
                    s => s.RequiresPermit),

            };

            RawName = system.RawName;
            Sources = new List<Source> { source };
        }



        public string Name { get { return GetField<string>("Name"); } }
        public string Government { get { return GetField<string>("Government"); } }
        public string Allegiance { get { return GetField<string>("Allegiance"); } }
        public double? Population { get { return GetField<double?>("Population"); } }
        public string PrimaryEconomy { get { return GetField<string>("PrimaryEconomy"); } }

        private T GetField<T>(string fieldName)
        {
            var field = values.OfType<MatchValue<T, ISystem>>().FirstOrDefault(v => v.Name == fieldName);
            return field == null ? default(T) : field.Value;
        }

        public void Update(ISystem entity, Source source, SharedData sharedData)
        {
            Sources.Add(source);

            foreach (var value in values)
            {
                value.Update(entity, source, sharedData);
            }
        }


        public void UpdateByConsensus(string reasonCode, ValueWithSources consensus)
        {
            var value = values.FirstOrDefault(v => v.Code == reasonCode);
            if (value == null) return;
            value.Set(consensus.Value, consensus.Sources);
            byConsensus = true;
        }

        public override string MatchLine
        {
            get
            {
                var allegiance = values.FirstOrDefault(v => v.Name == "Allegiance");
                var government = values.FirstOrDefault(v => v.Name == "Government");
                var economy = values.FirstOrDefault(v => v.Name == "PrimaryEconomy");
                return string.Join("\t", Name, allegiance.ValueAsString, government.ValueAsString, economy.ValueAsString, SourceForDisplay);
            }
        }

        public List<string> Updates(string baseProvider)
        {
            var changes = new List<string>();
            if (Sources.All(s => s.Who != baseProvider))
            {
                changes.Add(string.Join("\t", "ADDSYS", Name, "", SourceForDisplay));
            }
            else
            {
                foreach (var value in values)
                {
                    var newlySeen = value.Sources.All(s => s.Who != baseProvider);
                    if (value.Name == "Name")
                    {
                        if (newlySeen || Name != RawName)
                        {
                            changes.Add(string.Join("\t", "RENSYS", value.ValueAsString, RawName, value.SourcesForDisplay));
                        }
                    }
                    else if (newlySeen)
                    {
                        changes.Add(string.Join("\t", value.Code, Name, value.ValueAsString, value.SourcesForDisplay));
                    }
                }
            }
            return changes;
        }

    }
    
}