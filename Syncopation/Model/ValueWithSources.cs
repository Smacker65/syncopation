using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Syncopation.Model
{
    [DebuggerDisplay("{DebuggerDisplay,nq}")]
    public class ValueWithSources
    {
        public ValueWithSources(string value, List<Source> sources)
        {
            Value = value;
            Sources = sources;
        }

        public readonly string Value;
        public readonly List<Source> Sources;

        public List<string> Types { get { return Sources.Select(s => s.Type).Distinct().ToList(); } }


        // ReSharper disable once UnusedMember.Local
        private string DebuggerDisplay
        {
            get { return "ValueWithSources=" + ToString(); }
        }

        public override string ToString()
        {
            return string.Format("{0} From {1} [{2}]", Value, string.Join("+", Sources.Select(s => s.Who)), Types.Count);
        }
    }
}