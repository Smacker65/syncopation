﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Syncopation.View
{
    /// <summary>
    /// Interaction logic for Actionator.xaml
    /// </summary>
    public partial class Actionator : UserControl
    {
        public Actionator()
        {
            InitializeComponent();

            LayoutRoot.DataContext = this;
        }

        public ICommand Command
        {
            get { return (ICommand) GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public int ProgressValue
        {
            get { return (int) GetValue(ProgressValueProperty); }
            set { SetValue(ProgressValueProperty, value); }
        }

        public Visibility ProgressVisibility
        {
            get { return (Visibility) GetValue(ProgressVisibilityProperty); }
            set { SetValue(ProgressVisibilityProperty, value); }
        }

        public IEnumerable<string> Commands
        {
            get { return (IEnumerable<string>)GetValue(CommandsProperty); }
            set { SetValue(CommandsProperty, value); }
        }

        public int SelectedIndex
        {
            get { return (int) GetValue(SelectedIndexProperty); }
            set { SetValue(SelectedIndexProperty, value); }
        }

        public IEnumerable<string> SystemProviders
        {
            get { return (IEnumerable<string>) GetValue(SystemProvidersProperty); }
            set { SetValue(SystemProvidersProperty, value); }
        }

        public string BaseProvider
        {
            get { return (string) GetValue(BaseProviderProperty); }
            set { SetValue(BaseProviderProperty, value); }
        }

        public string UpdateCount
        {
            get { return (string) GetValue(UpdateCountProperty); }
            set { SetValue(UpdateCountProperty, value); }
        }

        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register("Command", typeof(ICommand), typeof(Actionator), new PropertyMetadata(default(ICommand)));
        public static readonly DependencyProperty ProgressValueProperty = DependencyProperty.Register("ProgressValue", typeof (int), typeof (Actionator), new PropertyMetadata(default(int)));
        public static readonly DependencyProperty ProgressVisibilityProperty = DependencyProperty.Register("ProgressVisibility", typeof (Visibility), typeof (Actionator), new PropertyMetadata(default(Visibility)));
        public static readonly DependencyProperty CommandsProperty = DependencyProperty.Register("Commands", typeof(IEnumerable<string>), typeof(Actionator), new PropertyMetadata(default(IEnumerable<string>)));
        public static readonly DependencyProperty SelectedIndexProperty = DependencyProperty.Register("SelectedIndex", typeof (int), typeof (Actionator), new PropertyMetadata(default(int)));
        public static readonly DependencyProperty SystemProvidersProperty = DependencyProperty.Register("SystemProviders", typeof (IEnumerable<string>), typeof (Actionator), new PropertyMetadata(default(IEnumerable<string>)));
        public static readonly DependencyProperty BaseProviderProperty = DependencyProperty.Register("BaseProvider", typeof (string), typeof (Actionator), new PropertyMetadata(default(string)));
        public static readonly DependencyProperty UpdateCountProperty = DependencyProperty.Register("UpdateCount", typeof (string), typeof (Actionator), new PropertyMetadata(default(string)));
    }
}
