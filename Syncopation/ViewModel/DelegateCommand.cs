﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Input;

namespace Syncopation.ViewModel
{
    public class DelegateCommand : ICommand
    {
        private readonly Action<object> execute;
        private readonly Predicate<object> canExecute;
        private readonly Action<int> setProgress;
        private readonly Action<string> setProgressText;

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        /// <param name="setProgress"></param>
        /// <param name="visibilityEvent"></param>
        public DelegateCommand(Action<object> execute, Predicate<object> canExecute = null, Action<int> setProgress = null, string visibilityEvent = null, Action<string> setProgressText = null)
        {
            VisibilityEvent = visibilityEvent;
            if (execute == null) throw new ArgumentNullException("execute");

            this.execute = execute;
            this.canExecute = canExecute;
            this.setProgress = setProgress;
            this.setProgressText = setProgressText;
        }

        public BackgroundWorker Worker { get; set; }

        public string VisibilityEvent { get; private set; }

        private int progress;
        private string progressText;

        public int Progress
        {
            get { return progress; }
            set
            {
                if (value == progress) return;
                progress = value;
                setProgress(progress);
            }
        }

        public string ProgressText
        {
            get { return progressText; }
            set
            {
                if (value == progressText) return;
                progressText = value;
                setProgressText(progressText);
            }
        }

        [DebuggerStepThrough]
        public bool CanExecute(object parameter)
        {
            return canExecute == null || canExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            execute(parameter);
        }
    }
}
