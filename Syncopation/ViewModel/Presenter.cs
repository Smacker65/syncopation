﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Syncopation.Model;

namespace Syncopation.ViewModel
{
    public class Presenter : ObservableObject
    {
        private readonly ObservableCollection<string> history;
        private readonly List<RunInfo> workers;

        public Presenter()
        {
            history = new ObservableCollection<string>();
            workers = new List<RunInfo>();
        }

        public IEnumerable<string> History
        {
            get { return history; }
        }

        private static Visibility ProgressVisibility(DelegateCommand command)
        {
            return command.Progress>0 || (command.Worker != null && command.Worker.IsBusy) ? Visibility.Visible : Visibility.Hidden;
        }

        private bool ReadyToRun(BackgroundWorker worker)
        {
            return worker == null || !worker.IsBusy;
        }



        private DelegateCommand starUpdate;
        public ICommand StarUpdate
        {
            get
            {
                return starUpdate ?? (starUpdate =
                    new DelegateCommand(
                                        p => RunBackground(new RunInfo(new StarChecker(), StarUpdateProvider, starUpdate, StarUpdateIndex > 0)),
                                        p => ReadyToRun(starUpdate.Worker),
                                        p => StarUpdateProgress = starUpdate.Progress,
                                        "StarUpdateProgressVisibility",
                                        p => StarUpdateUpdateCount = starUpdate.ProgressText
                                     ));
            }
        }

        private int starUpdateProgress;
        public int StarUpdateProgress
        {
            get { return starUpdateProgress; }
            private set
            {
                starUpdateProgress = value;
                RaisePropertyChangedEvent();
            }
        }

        public Visibility StarUpdateProgressVisibility
        {
            get
            {
                return ProgressVisibility(starUpdate);
            }
        }


        public IEnumerable<string> StarUpdateCommands
        {
            get { return new[] { "Star Check", "Star Update" }; }
        }

        public int StarUpdateIndex { get; set; }

        public string StarUpdateProvider { get; set; }

        private string starUpdateUpdateCount;
        public string StarUpdateUpdateCount
        {
            get { return starUpdateUpdateCount; }
            private set
            {
                starUpdateUpdateCount = value;
                RaisePropertyChangedEvent();
            }
        }
        private DelegateCommand syncopate;
        public ICommand Syncopate
        {
            get
            {
                return syncopate ?? (syncopate =
                    new DelegateCommand(
                                        p => RunBackground(new RunInfo(new Syncopator(), SyncopateProvider, syncopate, SyncopateIndex > 0)),
                                        p => ReadyToRun(syncopate.Worker),
                                        p => SyncopateProgress = syncopate.Progress,
                                        "SyncopateProgressVisibility",
                                        p => SyncopateUpdateCount = syncopate.ProgressText
                                     ));
            }
        }

        private int syncopateProgress;
        public int SyncopateProgress
        {
            get { return syncopateProgress; }
            private set
            {
                syncopateProgress = value;
                RaisePropertyChangedEvent();
            }
        }

        public Visibility SyncopateProgressVisibility
        {
            get
            {
                return ProgressVisibility(syncopate);
            }
        }

        public IEnumerable<string> SyncopateCommands
        {
            get { return new[] { "Multi-Compare", "Syncopate" }; }
        }

        public int SyncopateIndex { get; set; }

        public string SyncopateProvider { get; set; }

        private string syncopateUpdateCount;
        public string SyncopateUpdateCount
        {
            get { return syncopateUpdateCount; }
            private set
            {
                syncopateUpdateCount = value;
                RaisePropertyChangedEvent();
            }
        }

        private DelegateCommand moveSystems;
        public ICommand MoveSystems
        {
            get
            {
                return moveSystems ?? (moveSystems =
                    new DelegateCommand(
                                        p => MoveSys(),
                                        p => true
                                     ));
            }
        }

        private void MoveSys()
        {
            var utils = new SharedData();
            var starChartUtilities = new StarChartUtilities(utils);
            starChartUtilities.Message += SyncopatorMessage;
            starChartUtilities.Move("North America Sector NN-T c3-21", new Position(-1893.75, -15.78125, 179.75), "Syncopation");
        }

        private DelegateCommand deleteSystems;
        public ICommand DeleteSystems
        {
            get
            {
                return deleteSystems ?? (deleteSystems =
                    new DelegateCommand(
                                        p => DeleteSys(),
                                        p => true
                                     ));
            }
        }

        private void DeleteSys()
        {
            var edit = new EditRequest("http://starchart.club/api/edit");
            edit.DeleteSystem(new Position(-80, 3, -10));
            edit.DeleteSystem(new Position(-145.0625, 63.28125, -33.875));
            edit.DeleteSystem(new Position(42.0625, 55.6875, -9.9375));
            edit.DeleteSystem(new Position(-140.84375, 45.9375, -12.53125));
            edit.DeleteSystem(new Position(-141.9375, 45.75, -10.46875));
            edit.DeleteSystem(new Position(-130.84375, 41.8125, -21.78125));
            edit.DeleteSystem(new Position(-98.71875, 22.5, -60.34375));
            edit.DeleteSystem(new Position(-143.3125, 49.75, -45.3125));
            edit.DeleteSystem(new Position(-173.875, 2.5625, -31.21875));
            edit.DeleteSystem(new Position(-123.53125, 42.3125, -72.71875));
            edit.DeleteSystem(new Position(-152.125, -1.71875, -49.34375));
            edit.DeleteSystem(new Position(-87.65625, 0.59375, -41.53125));
            edit.DeleteSystem(new Position(-97, 14.15625, -33.5));
            edit.DeleteSystem(new Position(-126.40625, 20.09375, -57.3125));
            edit.DeleteSystem(new Position(-167.09375, 26.71875, -55.375));
            edit.DeleteSystem(new Position(-142, 67.625, -36.34375));
            edit.DeleteSystem(new Position(-157.0625, 33.1875, -69.8125));
            edit.DeleteSystem(new Position(-129.8125, 37.34375, -48.78125));
            edit.DeleteSystem(new Position(-69.65625, 67.3125, -39.03125));
            edit.DeleteSystem(new Position(-107.9375, 5.96875, -47.71875));
            edit.DeleteSystem(new Position(-133.25, 61.9375, -38.09375));
            edit.DeleteSystem(new Position(-27.90625, 61.21875, -11.125));
            edit.DeleteSystem(new Position(-100.15625, 35.8125, -62.0625));
            edit.DeleteSystem(new Position(-126, 29.21875, -4));
            edit.DeleteSystem(new Position(-184, 44.78125, -58.1875));
            edit.DeleteSystem(new Position(-121.96875, 16.03125, -71.40625));
            edit.DeleteSystem(new Position(-176.25, 30.75, -71.59375));
            edit.DeleteSystem(new Position(-163.0625, 14.03125, -64.5625));
            edit.DeleteSystem(new Position(74.53125, 40.125, -20.4375));
            edit.DeleteSystem(new Position(-171.0625, 43.28125, -34.96875));
            edit.DeleteSystem(new Position(-83.375, 21.5625, -41.09375));
            edit.DeleteSystem(new Position(32.25, 14.90625, -13.5));
            edit.DeleteSystem(new Position(-172.875, 11.25, -51.125));
            edit.DeleteSystem(new Position(-154.65625, 40.34375, -82.78125));
            edit.DeleteSystem(new Position(1.0625, 3.875, -9));
            edit.DeleteSystem(new Position(52.96875, 51.0625, -16.65625));
            edit.DeleteSystem(new Position(-141.78125, 46.34375, -18.25));
            
            // Test -115, -169.9375, 79.34375
            // Test2 -36.59375, 312.9375, 55.9375

            //Giguru Position(-80, 3, -10)
            //Zopiates Position(-145.0625, 63.28125, -33.875)
            //Zangi Position(42.0625, 55.6875, -9.9375)
            //Yakan Position(-140.84375, 45.9375, -12.53125)
            //Widjigarasir Position(-141.9375, 45.75, -10.46875)
            //Widjararappan Position(-130.84375, 41.8125, -21.78125)
            //Verboni Position(-98.71875, 22.5, -60.34375)
            //Shama Position(-143.3125, 49.75, -45.3125)
            //Seber Position(-173.875, 2.5625, -31.21875)
            //Sanguru Position(-123.53125, 42.3125, -72.71875)
            //Rabates Position(-152.125, -1.71875, -49.34375)
            //Nuwang Position(-87.65625, 0.59375, -41.53125)
            //Nener Position(-97, 14.15625, -33.5)
            //Mobokomu Position(-126.40625, 20.09375, -57.3125)
            //Markanomovoy Position(-167.09375, 26.71875, -55.375)
            //Mannheim Position(-142, 67.625, -36.34375)
            //Maitaokona Position(-157.0625, 33.1875, -69.8125)
            //Kurngali Position(-129.8125, 37.34375, -48.78125)
            //Kuikian Batji Position(-69.65625, 67.3125, -39.03125)
            //Kishia Position(-107.9375, 5.96875, -47.71875)
            //Keling Position(-133.25, 61.9375, -38.09375)
            //Katae Position(-27.90625, 61.21875, -11.125)
            //Kassi Hua Position(-100.15625, 35.8125, -62.0625)
            //Jun Position(-126, 29.21875, -4)
            //Jula Oh Position(-184, 44.78125, -58.1875)
            //Janjak Position(-121.96875, 16.03125, -71.40625)
            //Igororai Position(-176.25, 30.75, -71.59375)
            //Haeditjaray Position(-163.0625, 14.03125, -64.5625)
            //Feng Huang Position(74.53125, 40.125, -20.4375)
            //Cavashira Position(-171.0625, 43.28125, -34.96875)
            //Caluayaksheper Position(-83.375, 21.5625, -41.09375)
            //Caeronthudti Position(32.25, 14.90625, -13.5)
            //Binbeal Position(-172.875, 11.25, -51.125)
            //Aurvandill Position(-154.65625, 40.34375, -82.78125)
            //Arin Position(1.0625, 3.875, -9)
            //Apalai Position(52.96875, 51.0625, -16.65625)
            //Anaiwal Position(-141.78125, 46.34375, -18.25)

            edit.Execute();
        }

        private DelegateCommand clearMessages;
        public ICommand ClearMessages
        {
            get
            {
                return clearMessages ?? (clearMessages =
                    new DelegateCommand(
                                        p => history.Clear(),
                                        p => true
                                     ));
            }
        }


        private DelegateCommand renameSystems;
        public ICommand RenameSystems
        {
            get
            {
                return renameSystems ?? (renameSystems =
                    new DelegateCommand(
                                        p => RenameSys(),
                                        p => true
                                     ));
            }
        }

        public IEnumerable<string> SystemProviders
        {
            get { return new[] { "Starchart", "EDDB" }; }
        }

        private void RenameSys()
        {
            var renames = new Dictionary<string, string>
                          {
//{"ARIETIS SECTOR WO-R 84-4", "Arietis Sector WO-R b4-4"},
//{"Col 285 Sector GU-T 818-2", "Col 285 Sector GU-T b18-2"},
//{"Col 285 Sector GU-T 818-5", "Col 285 Sector GU-T b18-5"},
//{"Col 285 Sector HU-T 818-1", "Col 285 Sector HU-T b18-1"},
//{"COL 285 SECTOR QY-N B2 1-3", "Col 285 Sector QY-N b21-3"},
//{"Col 285 System MV-F b11-0", "Col 285 Sector MV-F b11-0"},
//{"Col 285 System MV-F b11-1", "Col 285 Sector MV-F b11-1"},
//{"Col 285 System MV-F b11-4", "Col 285 Sector MV-F b11-4"},
//{"Core Sys Sector WO-R A 4-3", "Core Sys Sector WO-R a4-3"},
//{"Cruscic Sector EL-Y c19", "Crucis Sector EL-Y c19"},
//{"Crusis Sector MD-S b4-3", "Crucis Sector MD-S b4-3"},
//{"Crusis Sector ND-S b4-1", "Crucis Sector ND-S b4-1"},
//{"Hyades Sector VO-Q v5-1", "Hyades Sector VO-Q b5-1"},
//{"Piscium Sector BM-L AB-1", "Piscium Sector BM-L a8-1"},
//{"Shu Wei Sector KN-S b4-4", "Shui Wei Sector KN-S b4-4"},
//{"Shu Wei Sector MN-S b4-4", "Shui Wei Sector MN-S b4-4"},
//{"Shu Wei Sector MN-S b4-9", "Shui Wei Sector MN-S b4-9"},
//{"Snuefe RO-Q c21-6", "Synuefe RO-Q c21-6"},
//{"WISE 0410+1502", "WISE 0410+ 1502"},
//{"WISE 2200-3628", "WISE 2220-3628"},
//{"Alani", "Alrai Sector DL-Y d116"},
//{"Tavytere", "Alrai Sector ON-T b3-2"},
//{"Njung", "Col 285 Sector VT-I c9-2"},
//{"Hydrae Sector IR-W b1-5", "Crucis Sector ER-V b2-5"},
//{"Hydrae Sector IM-W c1-26", "Crucis Sector EW-W c1-26"},
//{"Hydrae Sector KM-W b1-4", "Crucis Sector GM-V b2-4"},
//{"G 146-60", "G146-60"},
//{"Djali", "Herculis Sector QD-T b3-4"},
//{"ICW IW-W c1-13", "ICZ IW-W c1-13"},
//{"ICZ EW-V B2-E", "ICZ EW-V b2-3"},
//{"ICZ FW-V b2-6", "ICZ FW-V b2-2"},
//{"ICV HR-V b2-0", "ICZ HR-V b2-0"},
//{"Core Sys Sector HH-V b2-7", "Jastreb Sector PT-R b4-7"},
//{"Pleiades Sector WU-O b16-0", "Pleiades Sector WU-O b6-0"},
//{"Core Sys Sector DL-Y d117", "Puppis Sector DL-Y d117"},
//{"Core Sys Sector CQ-Y c22", "Puppis Sector FW-W c1-22"},
//{"Core Sys Sector NC-V b2-6", "Puppis Sector QD-T b3-6"},
//{"Sharru Sector SD-T b3-6", "Puppis Sector NN-T b3-6"},
//{"Angelaga", "Yin Sector XJ-A c21"},
//{"lhs 3583", "LHS 3583"},
//{"lp 658-44", "LP 658-44"},
//{"Col 285 Sector GI-P b20-4 A", "Col 285 Sector GI-P b20-4"},
//{"Alrai Sector OI-T b3-6 A", "Alrai Sector OI-T b3-6"},
//{"r Canis Majoris", "R Canis Majoris"},
//{"Wise 0350-5658", "WISE 0350-5658"},
//{"Wise 0359-5401", "WISE 0359-5401"},
//{"Alrai Sector JC-V b2-6 A", "Alrai Sector JC-V b2-6"},
//{"Harermid", "Haremid"},
//{"Piscium Sector B4-2", "Piscium Sector PT-R b4-2"},
//{"Puppis Sector EG-X b1-1 A", "Puppis Sector EG-X b1-1"},
//{"Yin Sector EL-Y b6", "Tascheter Sector FB-X b1-6"},
//{"Col 285 Sector QE-M b22-6", "HIP 72673"},
//{"Haremid", "Harermid"}
//{"Bodb Djedi", "Bodedi"},
//{"HIP 101110", "New Yembo"},
//{"Hydrae Sector IR-W b1-2", "Crucis Sector ER-V b2-2"},
//{"Puppis Sector ZZ-Y b4", "Puppis Sector CQ-Y c18"},
//{"OI-T B3-9", "Alrai Sector OI-T b3-9"},
//{"25 Chi Capricorni", "25 chi Capricorni"},
//{"44 Chi Draconis", "44 chi Draconis"},
//{"48 Chi Bootis", "48 chi Bootis"},
//{"53 Chi Ceti", "53 chi Ceti"},
//{"63 Chi Leonis", "63 chi Leonis"},
//{"ALPHA MUSCAE", "Alpha Muscae"},
//{"BETA MUSCAE", "Beta Muscae"},
//{"Cephei Sector KM-W C1-24", "Cephei Sector KM-W c1-24"},
//{"Cephei Sector NX-U B2-6", "Cephei Sector NX-U b2-6"},
//{"Cephei Sector NX-U B2-7", "Cephei Sector NX-U b2-7"},
//{"Col 285 Sector LE-E B13-1", "Col 285 Sector LE-E b13-1"},
//{"EPSILON CRUCIS", "Epsilon Crucis"},
//{"GAMMA TRIANGULI AUSTRALIS", "Gamma Trianguli Australis"},
//{"Ic 2602 Sector CB-X d1-151", "IC 2602 Sector CB-X d1-151"},
//{"Ic 2602 Sector FW-W d1-48", "IC 2602 Sector FW-W d1-48"},
//{"Ic 4604 Sector QD-T b3-2", "IC 4604 Sector QD-T b3-2"},
//{"Lbn 623 Sector FW-W d1-101", "LBN 623 Sector FW-W d1-101"},
//{"M CARINAE", "M Carinae"},
//{"m Hydrae", "M Hydrae"},
//{"p Eridani", "P Eridani"},
//{"Q CENTAURI", "Q Centauri"},
//{"R Cra Sector GC-L b8-0", "R CrA Sector GC-L b8-0"},
//{"r Doradus", "R Doradus"},
//{"ZETA-1 MUSCAE", "Zeta-1 Muscae"},
//{"19 LAMBDA BOOTIS","19 Lambda Bootis"},
//{"ADARA","Adara"},
//{"BATARA KALA","Batara Kala"},
//{"ED ASICH","Ed Asich"},
//{"EK DRACONIS","EK Draconis"},
//{"FN BOOTIS","FN Bootis"},
//{"LIR","Lir"},
//{"HOANDCAN","Hoandcan"},
//{"ROSS 271","Ross 271"},
//    {"ALRAI SECTOR IM-V B2-3", "Alrai Sector IM-V b2-3"},
//    {"Alrai Sector UI-T A3-1", "Alrai Sector UI-T a3-1"},
//    {"Alrai Sector ZU-P A5-4", "Alrai Sector ZU-P a5-4"},
//    {"Col 285 Sector DC-R B19-3", "Col 285 Sector DC-R b19-3"},
//    {"Col 285 Sector DC-R B19-7", "Col 285 Sector DC-R b19-7"},

//{"Col 285 Sector EC-R B19-5", "Col 285 Sector EC-R b19-5"},
//{"col 285 sector hk-j b24-4", "Col 285 Sector HK-J b24-4"},
//{"Col 285 Sector II-P B20-7", "Col 285 Sector II-P b20-7"},
//{"col 285 sector ik-j b24-2", "Col 285 Sector IK-J b24-2"},
//{"col 285 sector ik-j b24-4", "Col 285 Sector IK-J b24-4"},
//{"Col 285 Sector JI-P B20-0", "Col 285 Sector JI-P b20-0"},
//{"Col 285 Sector JI-P B20-3", "Col 285 Sector JI-P b20-3"},
//{"Col 285 Sector JI-P B20-4", "Col 285 Sector JI-P b20-4"},
//{"Col 285 Sector JI-P B20-8", "Col 285 Sector JI-P b20-8"},
//{"Col 285 Sector KP-G A40-3", "Col 285 Sector KP-G a40-3"},
//{"Col 285 Sector KP-G A40-4", "Col 285 Sector KP-G a40-4"},
//{"Col 285 Sector NZ-U B17-5", "Col 285 Sector NZ-U b17-5"},
//{"Col 285 Sector OK-P A35-0", "Col 285 Sector OK-P a35-0"},
//{"Col 285 Sector OK-P A35-2", "Col 285 Sector OK-P a35-2"},
//{"col 285 sector ta-d c13-19", "Col 285 Sector TA-D c13-19"},

//    {"Core Sys Sector CB-O A6-0", "Core Sys Sector CB-O a6-0"},
//    {"Core Sys Sector EB-O a6-0", "Core Sys Sector PI-T b3-7"},
//    {"Core Sys Sector TO-R A4-1", "Core Sys Sector TO-R a4-1"},
//    {"Core Sys Sector VJ-R A4-0", "Core Sys Sector VJ-R a4-0"},
//    {"Core Sys Sector VJ-R A4-1", "Core Sys Sector VJ-R a4-1"},
//    {"CORE SYS SECTOR ZP-P A5-1", "Core Sys Sector ZP-P a5-1"},
//    {"Core Sys Sector ZP-P A5-3", "Core Sys Sector ZP-P a5-3"},

//    {"ICZ DQ-Y C15", "ICZ DQ-Y c15"},
//    {"ICZ EL-X B1-1", "ICZ EL-X b1-1"},
//    {"ICZ IR-V B2-5", "ICZ IR-V b2-5"},
//    {"ICZ RO-Q B5-2", "ICZ RO-Q b5-2"},
//    {"Icz RO-Q B5-4", "ICZ RO-Q b5-4"},
//    {"ICZ SO-Q B5-2", "ICZ SO-Q b5-2"},
//    {"ICZ UJ-Q B5-1", "ICZ UJ-Q b5-1"},
//    {"ICZ UJ-Q B5-2", "ICZ UJ-Q b5-2"},

//{"Piscium Sector AG-W A2-1", "Piscium Sector AG-W a2-1"},
//{"Piscium Sector AG-W A2-2", "Piscium Sector AG-W a2-2"},
//{"Piscium Sector DM-U A3-1", "Piscium Sector DM-U a3-1"},
//{"Piscium Sector DM-U A3-2", "Piscium Sector DM-U a3-2"},
//{"Piscium Sector DM-U A3-3", "Piscium Sector DM-U a3-3"},
//{"Piscium Sector DR-V B2-6", "Piscium Sector DR-V b2-6"},
//{"Piscium Sector EM-U A3-0", "Piscium Sector EM-U a3-0"},
//{"Piscium Sector EM-U A3-1", "Piscium Sector EM-U a3-1"},
//{"Piscium Sector EM-U A3-2", "Piscium Sector EM-U a3-2"},
//{"Piscium Sector ZK-X B1-3", "Piscium Sector ZK-X b1-3"},

//    {"Scorpii Sector FQ-Y A1", "Scorpii Sector FQ-Y a1"},
//    {"Shui Wei Sector TU-O B6-7", "Shui Wei Sector TU-O b6-7"},

//    {"Tascheter Sector EW-W C1-29", "Tascheter Sector EW-W c1-29"},
//    {"Tascheter Sector FM-V B2-3", "Tascheter Sector FM-V b2-3"},
//    {"Tascheter Sector HH-V B2-5", "Tascheter Sector HH-V b2-5"},
//    {"Tascheter Sector HH-V B2-6", "Tascheter Sector HH-V b2-6"},
//    {"Tascheter Sector RE-Q A5-0", "Tascheter Sector RE-Q a5-0"},
//    {"Tascheter Sector RE-Q A5-3", "Tascheter Sector RE-Q a5-3"},

//    {"Tucanae Sector XE-Z B1", "Tucanae Sector XE-Z b1"},

//    {"Wolf 851 A", "Wolf 851"},

//{"Core Sys Sector EB-0 A6-0", "Core Sys Sector EB-O a6-0"},
//{"Pandamonium", "Pandemonium"},
//{"Kawa Atius", "Sharru Sector CQ-Y c10"},

//{"Flechs", "ICZ IR-W c1-13"},
//{"M Carinae", "m Carinae (HD 83944)"},

//{"SADR", "Sadr"},

//{"Col 285 Sector BW-1 B24-7", "Col 285 Sector BW-I b24-7"},
//{"Crucis Sector DB-0 A6-1", "Crucis Sector DB-O a6-1"},
//{"danae", "Danae"},
//{"Hip 37488", "HIP 37488"},
//{"hip 38892", "HIP 38892"},
//{"hip 83757", "HIP 83757"},
//{"Hr 7454", "HR 7454"},
//{"Hyades", "Hyades Sector HC-T b4-4"},
//{"Pleiades Sector SZ-0 B6-3", "Pleiades Sector SZ-O b6-3"},
//{"Pleiades Sector SZ-0 B6-4", "Pleiades Sector SZ-O b6-4"},
//{"porrima", "Porrima"},
//{"Wise 0713-2917", "WISE 0713-2917"},
//{"Argetlámh", "Argetlamh"},
//{"Den 0255-4700", "DEN 0255-4700"},
//{"Den 1048-3956", "DEN 1048-3956"},
//{"Do Canum Venaticorum", "DO Canum Venaticorum"},
//{"FURUHJELM II-431", "Furuhjelm II-431"},
//{"Lífthruti", "Lifthruti"},
//{"Mantóac", "Mantoac"},
//{"MU Cancri", "mu Cancri"},
//{"Nantóac", "Nantoac"},
//{"Praea Euq GR-O b13-0", "Praea Euq GR-D b13-0"},
//{"Bei Dou Sector JC-V b2-0", "Bei Dou Sector JV-C b2-2"},
//{"Bei Dou Sector JV-C b2-2", "Bei Dou Sector JC-V b2-0"},
//{"38 CETI", "38 Ceti"},
//{"46 VIRGINIS", "46 Virginis"},
//{"Plaa Eurik AX-O b12-0", "Plaa Eurk AX-D b12-0"},
//{"Plaa Eurik BA-Q b19-0", "Plaa Eurk BA-Q b19-0"},
//{"Plaa Eurik BW-W b15-0", "Plaa Eurk BW-W b15-0"},
//{"Plaa Eurik CA-Q b19-1", "Plaa Eurk CA-Q b19-1"},
//{"Plaa Eurik DW-W b15-0", "Plaa Eurk DW-W b15-0"},
//{"Plaa Eurik GK-R d4-33", "Plaa Eurk GK-R d4-33"},
//{"Plaa Eurik GV-P c5-4", "Plaa Eurk GV-P c5-4"},
//{"Plaa Eurik HH-V b16-0", "Plaa Eurk HH-V b16-0"},
//{"Plaa Eurik HK-R d4-16", "Plaa Eurk HK-R d4-16"},
//{"Plaa Eurik HK-R d4-6", "Plaa Eurk HK-R d4-6"},
//{"Plaa Eurik ID-C b13-0", "Plaa Eurk ID-C b13-0"},
//{"Plaa Eurik MV-P c5-1", "Plaa Eurk MV-P c5-1"},
//{"Plaa Eurik MV-P c5-6", "Plaa Eurk MV-P c5-6"},
//{"Plaa Eurik NO-A b14-0", "Plaa Eurk NO-A b14-0"},
//{"Plaa Eurik OB-O c6-3", "Plaa Eurk OB-O c6-3"},
//{"Plaa Eurik OB-O c6-4", "Plaa Eurk OB-O c6-4"},
//{"Plaa Eurik OG-E b12-0", "Plaa Eurk OG-E b12-0"},
//{"Plaa Eurik OI-T b17-1", "Plaa Eurk OI-T b17-1"},
//{"Plaa Eurik PX-U d2-12", "Plaa Eurk PX-U d2-12"},
//{"Plaa Eurik QJ-A b14-0", "Plaa Eurk QJ-A b14-0"},
//{"Plaa Eurik RA-H c10-5", "Plaa Eurk RA-H c10-5"},
//{"Plaa Eurik RB-E b12-1", "Plaa Eurk RB-E b12-1"},
//{"Plaa Eurik RB-O c6-0", "Plaa Eurk RB-O c6-0"},
//{"Plaa Eurik RX-U d2-13", "Plaa Eurk RX-U d2-13"},
//{"Plaa Eurik TA-H c10-3", "Plaa Eurk TA-H c10-3"},
//{"Plaa Eurik TO-R b18-1", "Plaa Eurk TO-R b18-1"},
//{"Plaa Eurik UO-R b18-1", "Plaa Eurk UO-R b18-1"},
//{"Plaa Eurik VO-R b18-1", "Plaa Eurk VO-R b18-1"},
//{"Plaa Eurik WO-R b18-0", "Plaa Eurk WO-R b18-0"},
//{"Plaa Eurik WP-Y b14-0", "Plaa Eurk WP-Y b14-0"},
//{"Plaa Eurik WW-D b12-0", "Plaa Eurk WW-D b12-0"},
//{"Plaa Eurik ZC-C b13-0", "Plaa Eurk ZC-C b13-0"},
//{"Plaa Eurik ZH-M c7-3", "Plaa Eurk ZH-M c7-3"},
//{"Col 285 Sector BN-Z c 14-8", "Col 285 Sector BN-Z c14-8"},
//{"Col 285 Sector LP-S B-19-1", "Col 285 Sector LP-S b19-1"},
//{"Col 285 Sector LP-S B-19-2", "Col 285 Sector LP-S b19-2"},
//{"Alrai Sector CL-W c1-14", "Alrai Sector GW-W c1-14"},
{"F PUPPIS 35393", "F Puppis 35393"},
{"Alrai Sector JW-B a6-1", "Alrai Sector JW-N a6-1"},
{"Vela Dark Region CL-L b24-7", "Col 173 Sector CR-L b24-7"},
{"Zhao Chim", "Hyades Sector GR-V c2-12"},
{"Shatrudnir", "ICZ GW-W c1-20"},
{"Vela Dark Region CR-L b24-5", "Col 173 Sector CR-L b24-5"},
{"Vela Dark Region CT-F c12-28", "Col 173 Sector CT-F c12-28"},
{"Vela Dark Region GX-J b25-2", "Col 173 Sector GX-J b25-2"},
                          };

            var utils = new SharedData();
            var starChartUtilities = new StarChartUtilities(utils);
            starChartUtilities.Message += SyncopatorMessage;
            starChartUtilities.Rename(renames, "Rename");
        }


        private class RunInfo
        {
            public RunInfo(ISyncopatorCommand command, string baseProvider, DelegateCommand delegateCommand, bool write)
            {
                BaseProvider = baseProvider;
                Command = command;
                DelegateCommand = delegateCommand;
                Write = write;
            }

            public ISyncopatorCommand Command { get; private set; }
            public DelegateCommand DelegateCommand { get; private set; }
            public bool Write { get; private set; }
            public string Message { get; set; }
            public double Progress { get; set; }
            public int Changes { get; set; }
            public string BaseProvider { get; private set; }
        }

        private void RunBackground(RunInfo runInfo)
        {
            runInfo.DelegateCommand.Worker = new BackgroundWorker {WorkerReportsProgress = true};
            runInfo.DelegateCommand.Worker.DoWork += WorkerOnDoWork;
            runInfo.DelegateCommand.Worker.RunWorkerCompleted += BackgroundWorkerOnRunWorkerCompleted;
            runInfo.DelegateCommand.Worker.ProgressChanged += BackgroundWorkerOnProgressChanged;

            runInfo.DelegateCommand.Worker.RunWorkerAsync(runInfo);
        }

        private void BackgroundWorkerOnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs runWorkerCompletedEventArgs)
        {
            var runInfo = runWorkerCompletedEventArgs.Result as RunInfo;
            if (runInfo == null) return;
            if (runInfo.Write && runInfo.Changes>0)
            {
                var dispatcherTimer = new DispatcherTimer();
                dispatcherTimer.Tick += dispatcherTimer_Tick;
                dispatcherTimer.Interval = new TimeSpan(0, 0, 3);
                dispatcherTimer.Tag = runInfo;
                runInfo.DelegateCommand.Progress = 100;
                runInfo.DelegateCommand.ProgressText = runInfo.Changes.ToString();
                dispatcherTimer.Start();
            }
            else
            {
                runInfo.DelegateCommand.Progress = 0;
                // ReSharper disable once ExplicitCallerInfoArgument
                RaisePropertyChangedEvent(runInfo.DelegateCommand.VisibilityEvent);
            }
            runInfo.DelegateCommand.CanExecute(null);
            CommandManager.InvalidateRequerySuggested();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            var timer = sender as DispatcherTimer;
            if (timer == null) return;
            var runInfo = timer.Tag as RunInfo;
            if (runInfo == null)
            {
                timer.Stop();
            }
            else
            {
                runInfo.DelegateCommand.Progress -= 1;
                if (runInfo.DelegateCommand.Progress <= 0)
                {
                    timer.Stop();
                    // ReSharper disable once ExplicitCallerInfoArgument
                    RaisePropertyChangedEvent(runInfo.DelegateCommand.VisibilityEvent);
                }
            }
            CommandManager.InvalidateRequerySuggested();
        }

        private void BackgroundWorkerOnProgressChanged(object sender, ProgressChangedEventArgs progressChangedEventArgs)
        {
            var runInfo = progressChangedEventArgs.UserState as RunInfo;
            if (runInfo == null) return;
            if (progressChangedEventArgs.ProgressPercentage >= 0)
            {
                //Debug.WriteLine("Progress=" + progressChangedEventArgs.ProgressPercentage);
                runInfo.DelegateCommand.Progress = progressChangedEventArgs.ProgressPercentage;
            }
            if (!string.IsNullOrEmpty(runInfo.Message))
            {
                AddToHistory(runInfo.Message);
                runInfo.Message = null;
            }
            CommandManager.InvalidateRequerySuggested();
        }

        private void WorkerOnDoWork(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            var worker = sender as BackgroundWorker;
            if (worker == null) return;
            var runInfo = doWorkEventArgs.Argument as RunInfo;
            if (runInfo == null) return;
            // ReSharper disable once ExplicitCallerInfoArgument
            RaisePropertyChangedEvent(runInfo.DelegateCommand.VisibilityEvent);
            doWorkEventArgs.Result = runInfo;
            runInfo.Progress = 0;
            worker.ReportProgress(0, runInfo);
            runInfo.DelegateCommand.CanExecute(null);
            runInfo.DelegateCommand.ProgressText = "";
            runInfo.Command.SharedData = new SharedData();
            runInfo.Command.ProgressChanged += SyncopatorProgressChanged;
            runInfo.Command.Message += SyncopatorMessage;
            workers.Remove(workers.FirstOrDefault(w => w.Command.Name == runInfo.Command.Name));
            workers.Add(runInfo);
            runInfo.Changes = runInfo.Command.Run(runInfo.BaseProvider, runInfo.Write);
            worker.ReportProgress(100, runInfo);
        }


        private void SyncopatorProgressChanged(object sender, FineProgressChangedEventArgs e)
        {
            var id = e.UserState as string;
            if (string.IsNullOrEmpty(id)) return;
            var runInfo = workers.FirstOrDefault(w => w.Command.Name == id);
            if (runInfo == null) return;
            runInfo.Progress += e.ProgressPercentage;
            runInfo.DelegateCommand.Worker.ReportProgress((int)runInfo.Progress, runInfo);
        }

        private void SyncopatorMessage(object sender, MessageEventArgs e)
        {
            var id = e.UserState as string;
            if (string.IsNullOrEmpty(id)) return;
            var runInfo = workers.FirstOrDefault(w => w.Command.Name == id);
            var message = string.Format("{0}: {1} {2}", DateTime.Now.ToString("HH:mm:ss"), id, e.Message);
            if (runInfo == null)
            {
                AddToHistory(message);
            }
            else
            {
                runInfo.Message = message;
                runInfo.DelegateCommand.Worker.ReportProgress(-1, runInfo);
            }
        }

        private void AddToHistory(string item)
        {
            history.Insert(0, item);
            CommandManager.InvalidateRequerySuggested();
        }
    }

}
