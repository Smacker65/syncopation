﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Syncopation2.ViewModel
{
    public class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate{};

        protected void RaisePropertyChangedEvent([CallerMemberName] string memberName = null)
        {
            if (string.IsNullOrEmpty(memberName)) return;
            PropertyChanged(this, new PropertyChangedEventArgs(memberName));
        }
    }
}
