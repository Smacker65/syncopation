﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Syncopation2.Model;

namespace Syncopation2.ViewModel
{
    public class Presenter : ObservableObject
    {
        private readonly TextConverter textConverter;
        private string someText;
        private readonly ObservableCollection<string> history;

        public Presenter()
        {
            history = new ObservableCollection<string>();
            textConverter = new TextConverter(s => s.ToUpper());
        }

        public string SomeText
        {
            get { return someText; }
            set
            {
                someText = value;
                RaisePropertyChangedEvent();
            }
        }

        public IEnumerable<string> History
        {
            get { return history; }
        }

        private DelegateCommand multiCompare;
        public ICommand MultiCompare
        {
            get
            {
                return multiCompare ?? (multiCompare =
                    new DelegateCommand(
                                        p => ConvertText(),
                                        p => TextValid
                                       ));
            }
        }

        private bool TextValid
        {   
            get { return !String.IsNullOrWhiteSpace(SomeText); }
        }

        private void ConvertText()
        {
            AddToHistory(textConverter.ConvertText(SomeText));
            SomeText = String.Empty;
        }

        private void AddToHistory(string item)
        {
            if (!history.Contains(item)) history.Insert(0, item);
        }
    }
}
